import React, {Component} from 'react';
import {appColors, appHeight, appWidth, fonts, styles} from '../../styles';
import {BackHeader} from '../../shared/BackHeader';
import {
  Alert,
  FlatList,
  Image,
  ImageBackground,
  Linking,
  Pressable,
  StyleSheet,
  Text,
  View,
} from 'react-native';
import {
  changeLoading,
  changeLoggedIn,
  errorHandling,
} from '../../storage/actions/actions';
import {connect} from 'react-redux';
import {fetchUserTransactions} from '../../services/userService';
import {SimpleBackHeader} from '../../shared/SimpleBackHeader';

const TransactionItem = ({item}) => (
  <Pressable style={localStyles.listRow}>
    <View
      style={{
        flex: 1,
        borderWidth: 1,
        paddingHorizontal: 10,
        paddingVertical: 10,
        borderRadius: 5,
        backgroundColor: appColors.mainWhite,
        flexDirection: 'column',
      }}>
      <View
        style={{
          justifyContent: 'center',
          alignItems: 'center',
          marginBottom: 5,
        }}>
        <Text style={[localStyles.listTitle]}>Κατάστημα {item.legalName}</Text>
      </View>

      <View style={{justifyContent: 'center', alignItems: 'flex-start'}}>
        <Text style={[localStyles.listTitle, {fontSize: 14}]}>
          Ημ. Συναλλαγής {item.dateCreated}
        </Text>
      </View>

      <View style={{justifyContent: 'center', alignItems: 'flex-start'}}>
        <Text style={[localStyles.listTitle, {fontSize: 14}]}>
          Ποσό {item.price} €
        </Text>
      </View>

      <View style={{justifyContent: 'center', alignItems: 'flex-start'}}>
        <Text style={[localStyles.listTitle, {fontSize: 14}]}>
          Credits {item.credits}
        </Text>
      </View>
    </View>
  </Pressable>
);

class Transactions extends Component {
  state = {
    transactions: [],
    isLoading: true,
  };

  constructor(props) {
    super(props);
  }

  renderItem = ({item}) => <TransactionItem item={item} />;

  async componentDidMount() {
    try {
      this.props.changeLoading(true);
      const transactions = await fetchUserTransactions();
      this.setState({transactions});
      console.log(transactions);
    } catch (e) {
      this.props.errorHandling({visible: true, message: e.message});
    } finally {
      this.props.changeLoading(false);
      this.setState({isLoading: false});
    }
  }

  render() {
    return (
      <View style={styles.mainContainer}>
        <ImageBackground
          source={require('../../assets/images/backgrounds/mainBlack.png')}
          resizeMode="cover"
          style={styles.image}>
          <SimpleBackHeader title="Οι Συναλλαγές μου" />

          {!this.state.isLoading ? (
            <View style={{flex: 1}}>
              <FlatList
                style={{paddingHorizontal: 10}}
                data={this.state.transactions}
                ListEmptyComponent={
                  <View
                    style={{
                      flex: 1,
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}>
                    <Image
                      source={require('../../assets/images/no_trans.png')}
                    />
                  </View>
                }
                renderItem={this.renderItem}
                keyExtractor={item => item.id}
              />
            </View>
          ) : (
            <View style={{flex: 1}} />
          )}
        </ImageBackground>
      </View>
    );
  }
}

const localStyles = StyleSheet.create({
  listContainer: {
    flex: 0.6,
    marginTop: appHeight * 0.06,
    paddingHorizontal: 5,
  },

  listRow: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    paddingVertical: 10,
    borderBottomColor: appColors.mainWhite,
    // borderBottomWidth: 0.5,
  },
  listTitle: {
    fontSize: 18,
    color: appColors.black,
    fontFamily: fonts.light
  },
});

const mapStateToProps = state => {
  return {actions: state.actions};
};

const mapDispatchToProps = dispatch => {
  return {
    changeLoading: value => dispatch(changeLoading(value)),
    errorHandling: value => dispatch(errorHandling(value)),
    changeLoggedIn: value => dispatch(changeLoggedIn(value)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Transactions);
