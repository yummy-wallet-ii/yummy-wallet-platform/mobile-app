import React, {Component} from 'react';
import {
  changeLoading,
  changeLoggedIn,
  errorHandling,
  setCart,
  setOrder,
  setUser,
  showMessage,
} from '../../storage/actions/actions';
import {connect} from 'react-redux';
import {ScrollView, Text, View} from 'react-native';
import {appColors, appHeight, fonts} from '../../styles';
import {Button, IconButton} from 'react-native-paper';

class ProductDetails extends Component {
  state = {
    product: null,
    bottomSheetRef: null,
    storePublicKey: null,
    storeId: null,
  };
  componentDidMount() {
    this.setState({
      product: this.props.product,
      bottomSheetRef: this.props.bottomSheetRef,
      storePublicKey: this.props.storePublicKey,
      storeId: this.props.storeId,
    });
  }

  addProductToCart = () => {
    const cart = this.props.actions.cart;
    const storeCart = cart[this.state.storeId];
    const orderItem = {
      itemTempId: storeCart.orderItems.length + 1,
      itemId: this.state.product.id,
      itemCategoryId: this.state.product.categoryId,
      itemName: this.state.product.title,
      itemImage: this.state.product.imageBlob,
      itemStoreId: this.state.storeId,
      itemStorePublicKey: this.state.storePublicKey,
      itemPrice: this.state.product.hasDiscount
        ? +this.state.product.alternativePrice
        : +this.state.product.price,
    };
    storeCart.orderItems.push(orderItem);
    this.props.setCart(cart);

    this.state.bottomSheetRef.close();
    this.props.showMessage({
      visible: true,
      message: {
        body: `Το προϊόν ${orderItem.itemName} προσθέθηκε στο καλάθι σας`,
      },
    });
  };

  removeProductToCart = () => {
    const orders = [...this.props.actions.orders];
    orders.push(this.state.product);
    this.props.setOrder([]);
  };

  render() {
    return (
      <View style={{flexDirection: 'column', padding: 10}}>
        <View
          style={{
            flexDirection: 'row',
            marginBottom: 10,
          }}>
          <Text style={{fontSize: 18, fontWeight: '800', fontFamily: fonts.light}}>
            {this.props.product.title}
          </Text>
        </View>

        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'center',
            marginBottom: 10,
          }}>
          <Text style={{fontSize: 16, fontWeight: '600', alignSelf: 'center', fontFamily: fonts.light}}>
            Περιγραφή
          </Text>
        </View>
        <ScrollView style={{height: appHeight * 0.39}}>
          <View >
            <Text style={{fontFamily: fonts.light}}>{this.props.product.description}</Text>
          </View>
        </ScrollView>

        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'flex-end',
            alignItems: 'center',
          }}>
          {this.props.actions.isLoggedIn ? (
            <Button
              dark={true}
              style={{backgroundColor: appColors.mainBlue}}
              icon="cart"
              mode="contained"
              uppercase={false}
              onPress={() => this.addProductToCart()}>
              Προσθήκη στο καλάθι
            </Button>
          ) : null}


          {/*<Text style={{color: appColors.mainBlue}}>Προσθήκη στο καλάθι</Text>*/}
          {/*<IconButton*/}
          {/*  icon="cart"*/}
          {/*  size={20}*/}
          {/*  color={appColors.mainBlue}*/}
          {/*  onPress={() => this.addProductToCart()}*/}
          {/*/>*/}

          {/*<IconButton*/}
          {/*  icon="minus"*/}
          {/*  size={20}*/}
          {/*  onPress={() => this.removeProductToCart()}*/}
          {/*/>*/}
        </View>
      </View>
    );
  }
}

const mapStateToProps = state => {
  return {actions: state.actions};
};

const mapDispatchToProps = dispatch => {
  return {
    changeLoading: value => dispatch(changeLoading(value)),
    errorHandling: value => dispatch(errorHandling(value)),
    changeLoggedIn: value => dispatch(changeLoggedIn(value)),
    setUser: value => dispatch(setUser(value)),
    setOrder: value => dispatch(setOrder(value)),
    setCart: value => dispatch(setCart(value)),
    showMessage: value => dispatch(showMessage(value)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ProductDetails);
