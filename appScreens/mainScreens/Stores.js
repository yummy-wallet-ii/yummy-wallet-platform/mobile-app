import React, {Component} from 'react';
import {
  changeLoading,
  errorHandling,
  setStores,
} from '../../storage/actions/actions';
import {
  ImageBackground,
  RefreshControl,
  StyleSheet,
  Text,
  View,
} from 'react-native';
import {appColors, appHeight, appWidth, fonts, styles} from '../../styles';
import {ScrollView} from 'react-native-gesture-handler';
import {fetchStores} from '../../services/storeService';
import {StoresNearBySwiper} from './StoresNearBySwiper';
import {connect} from 'react-redux';
import {Button} from 'react-native-paper';
import Geolocation from '@react-native-community/geolocation';
import {
  getOneSignalPlayerId,
  sendPushNotification,
} from '../../services/oneSignalService';

class Stores extends Component {
  state = {
    suggestedStores: [],
    bestTaxStores: [],
    nearbyStores: [],
    foodStores: [],
    recommendedStores: [],
    coffeeStores: [],
    refreshing: false,
  };
  constructor(props) {
    super(props);
    //
    this.props.navigation.addListener('focus', async () => {
      await this.fetchStoresForMobile();
    });
  }

  fetchStoresForMobile = async () => {
    try {
      const stores = await fetchStores();
      this.props.setStores(stores);
      const nearbyStores = stores.filter(
        store => store.latitude && store.longitude,
      );
      const foodStores = stores.filter(store => store.categoryId === 3);
      const bestTaxStores = stores.sort((a, b) =>
        a.cashbackPercentage > b.cashbackPercentage
          ? 1
          : b.cashbackPercentage > a.cashbackPercentage
          ? -1
          : 0,
      );
      this.setState({bestTaxStores, nearbyStores, foodStores});
    } catch (e) {
      console.log(e);
    } finally {
    }
  };

  onRefresh = async () => {
    try {
      this.setState({refreshing: true});
      await this.fetchStoresForMobile();
    } catch (e) {
      this.setState({refreshing: false});
    } finally {
      this.setState({refreshing: true});
    }
  };

  async componentDidMount() {
    try {
      this.props.changeLoading(true);

      await this.fetchStoresForMobile();

      // const playerId = await getOneSignalPlayerId();
      // const storeId = 1;
      // Geolocation.watchPosition(
      //   position => {
      //     sendPushNotification(playerId, storeId, 40.593347, 22.951186);
      //   },
      //   error => console.log(error),
      //   {enableHighAccuracy: true, distanceFilter: 10},
      // );
    } catch (e) {
      this.props.errorHandling({visible: true, message: e.message});
    } finally {
      this.props.changeLoading(false);
    }
  }

  goToMaps() {
    this.props.navigation.navigate('maps');
  }

  render() {
    return (
      <View style={[styles.mainContainer]}>
        {/*<MainHeader title="Καταστήματα" />*/}
        <ImageBackground
          source={require('../../assets/images/backgrounds/mainBlack.png')}
          resizeMode="cover"
          style={styles.image}>
          <ScrollView
            style={{marginTop: 20, paddingHorizontal: appWidth * 0.02}}
            refreshControl={
              <RefreshControl
                refreshing={this.state.refreshing}
                onRefresh={() => this.onRefresh()}
              />
            }>
            <View style={localStyles.storeCardContainer}>
              <View style={{flexDirection: 'column', flex: 1}}>
                <View style={{flexDirection: 'row'}}>
                  <Text style={localStyles.sectionHeader}>
                    Σας προτείνουμε
                  </Text>
                </View>
                <View style={localStyles.cardRow}>
                  <StoresNearBySwiper stores={this.state.nearbyStores} />
                </View>
              </View>
            </View>

            {/*<View style={localStyles.storeCardContainer}>*/}
            {/*  <View style={{flexDirection: 'column', flex: 1}}>*/}
            {/*    <View style={{flexDirection: 'row'}}>*/}
            {/*      <Text style={localStyles.sectionHeader}>Σας προτείνουμε</Text>*/}
            {/*    </View>*/}
            {/*    <View style={localStyles.cardRow}>*/}
            {/*      <StoresNearBySwiper stores={this.state.recommendedStores} />*/}
            {/*    </View>*/}
            {/*  </View>*/}
            {/*</View>*/}

            {/*<View style={localStyles.storeCardContainer}>*/}
            {/*  <View style={{flexDirection: 'column'}}>*/}
            {/*    <View style={{flexDirection: 'row'}}>*/}
            {/*      <Text style={localStyles.sectionHeader}>Μόνο για σένα</Text>*/}
            {/*    </View>*/}
            {/*    <View style={{flexDirection: 'row'}}></View>*/}
            {/*  </View>*/}
            {/*</View>*/}

            <View style={localStyles.storeCardContainer}>
              <View style={{flexDirection: 'column', flex: 1}}>
                <View style={{flexDirection: 'row'}}>
                  <Text style={localStyles.sectionHeader}>
                    Προτεινόμενα για φαγητό
                  </Text>
                </View>
                <View style={localStyles.cardRow}>
                  <StoresNearBySwiper stores={this.state.foodStores} />
                </View>
              </View>
            </View>

            {/*<View style={localStyles.storeCardContainer}>*/}
            {/*  <View style={{flexDirection: 'column', flex: 1}}>*/}
            {/*    <View style={{flexDirection: 'row'}}>*/}
            {/*      <Text style={localStyles.sectionHeader}>*/}
            {/*        Προτεινόμενα για καφέ*/}
            {/*      </Text>*/}
            {/*    </View>*/}
            {/*    <View style={localStyles.cardRow}>*/}
            {/*      <StoresNearBySwiper stores={this.state.coffeeStores} />*/}
            {/*    </View>*/}
            {/*  </View>*/}
            {/*</View>*/}

            <View style={localStyles.storeCardContainer}>
              <View style={{flexDirection: 'column', flex: 1}}>
                <View style={{flexDirection: 'row'}}>
                  <Text style={localStyles.sectionHeader}>
                    Με τη μεγαλύτερη επιστροφή
                  </Text>
                </View>
                <View style={localStyles.cardRow}>
                  <StoresNearBySwiper stores={this.state.bestTaxStores} />
                </View>
              </View>
            </View>

            <View
              style={[
                localStyles.storeCardContainer,
                {height: appHeight * 0.2},
              ]}
            />
          </ScrollView>
        </ImageBackground>
      </View>
    );
  }
}

const localStyles = StyleSheet.create({
  storeCardContainer: {
    flexDirection: 'row',
    marginVertical: appHeight * 0.02,
    width: appWidth,
    // height: appHeight * 0.2,
    // borderBottomColor: 'lightgray',
    // borderBottomWidth: 1,
  },
  sectionHeader: {
    fontSize: 22,
    // fontWeight: '800',
    fontFamily: fonts.light,
    color: appColors.iceWhite
  },
  cardRow: {
    marginTop: 5,
    flexDirection: 'row',
    flex: 1,
  },
});

const mapStateToProps = state => {
  return {actions: state.actions};
};

const mapDispatchToProps = dispatch => {
  return {
    changeLoading: value => dispatch(changeLoading(value)),
    errorHandling: value => dispatch(errorHandling(value)),
    setStores: value => dispatch(setStores(value)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Stores);
