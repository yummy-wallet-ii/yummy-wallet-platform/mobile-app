import React from 'react';
import {Checkbox, Text, TextInput} from 'react-native-paper';
import { View, StyleSheet, ScrollView} from 'react-native';
import {appColors, appHeight, appWidth, fonts} from '../../styles';

const StoreBottomSheet = props => {
  // const handlePhoneCall = async phoneNumber => {
  //   const url = 'tel:' + phoneNumber;
  //   const supported = await Linking.canOpenURL(url);
  //
  //   if (supported) {
  //     await Linking.openURL(url);
  //   } else {
  //     this.props.errorHandling({
  //       visible: true,
  //       message:
  //         'Υπήρξε πρόβλημα στο άνοιγμα της εφαρμογής που διαχειρίζεται τις κλήσεις.',
  //     });
  //   }
  // };
  //
  // const handleLocation = async (lon, lat) => {
  //   await showLocation({latitude: +lat, longitude: +lon});
  // };

  const renderSchedule = () => {
    if (props.currentStore.schedule !== undefined) {
      const scheduleJson = JSON.parse(props.currentStore.schedule);
      return scheduleJson.map(item => {
        return (
          <View style={{flexDirection: 'row', flex: 1}} key={item.day}>
            <View
              style={{
                flexDirection: 'column',
                flex: 3,
                marginBottom: appHeight * 0.02,
              }}>
              <Text style={{textAlign: 'center', color: appColors.mainGray, fontFamily: fonts.light}}>
                {item.day}
              </Text>
            </View>
            <View style={{flexDirection: 'column', flex: 3}}>
              <Text style={{textAlign: 'center', color: appColors.mainGray, fontFamily: fonts.light}}>
                {item.start}
              </Text>
            </View>
            <View style={{flexDirection: 'column', flex: 3}}>
              <Text style={{textAlign: 'center', color: appColors.mainGray, fontFamily: fonts.light}}>
                {item.end}
              </Text>
            </View>
          </View>
        );
      });
    }
  };

  return (
    <View style={localStyles.container}>
      <ScrollView style={{flex: 1}}>
        <View style={{flexDirection: 'column'}}>
          <TextInput
            editable={false}
            value={props.currentStore.categoryDescription}
            left={
              <TextInput.Icon name="tag-text-outline" color={appColors.black} />
            }
            style={localStyles.textInput}
            theme={{colors: {text: appColors.black}}}
            underlineColor={appColors.black}
          />
          <TextInput
            editable={false}
            value={props.currentStore.headquarters}
            numberOfLines={3}
            left={
              <TextInput.Icon
                name="map-marker-outline"
                color={appColors.black}
              />
            }
            style={localStyles.textInput}
            theme={{colors: {text: appColors.black}}}
            underlineColor={appColors.black}
          />
          <TextInput
            editable={false}
            value={props.currentStore.telephone}
            left={
              <TextInput.Icon name="phone-outline" color={appColors.black} />
            }
            style={localStyles.textInput}
            theme={{colors: {text: appColors.black}}}
            underlineColor={appColors.black}
          />

          {props.currentStore.facebook ? (
            <TextInput
              editable={false}
              value={props.currentStore.facebook}
              left={<TextInput.Icon name="facebook" color={appColors.black} />}
              style={localStyles.textInput}
              theme={{colors: {text: appColors.black}}}
              underlineColor={appColors.black}
            />
          ) : null}

          {props.currentStore.instagram ? (
            <TextInput
              editable={false}
              value={props.currentStore.instagram}
              left={<TextInput.Icon name="instagram" color={appColors.black} />}
              style={localStyles.textInput}
              theme={{colors: {text: appColors.black}}}
              underlineColor={appColors.black}
            />
          ) : null}

          {props.currentStore.twitter ? (
            <TextInput
              editable={false}
              value={props.currentStore.twitter}
              left={<TextInput.Icon name="twitter" color={appColors.black} />}
              style={localStyles.textInput}
              theme={{colors: {text: appColors.black}}}
              underlineColor={appColors.black}
            />
          ) : null}

          {props.currentStore.website ? (
            <TextInput
              editable={false}
              value={props.currentStore.website}
              left={<TextInput.Icon name="web" color={appColors.black} />}
              style={localStyles.textInput}
              theme={{colors: {text: appColors.black}}}
              underlineColor={appColors.black}
            />
          ) : null}

          <View style={localStyles.scheduleContainer}>
            <View
              style={{justifyContent: 'center', alignItems: 'center', flex: 1}}>
              <Text style={localStyles.scheduleTitle}>Ωράριο λειτουργίας</Text>
            </View>

            {renderSchedule()}
          </View>

          <View
            style={{
              marginBottom: appHeight * 0.02,
              justifyContent: 'center',
              flex: 1,
            }}>
            <View style={{flex: 1, flexDirection: 'row', alignItems: 'center'}}>
              <Checkbox
                status={
                  props.currentStore.iBankPay === 1 ? 'checked' : 'unchecked'
                }
                color={appColors.mainBlue}
                uncheckedColor={appColors.mainGray}
              />
              <Text style={{color: appColors.black, fontFamily: fonts.light}}>
                Η επιχείρηση υποστηρίζει iBankPay
              </Text>
            </View>

            <View style={{flex: 1, flexDirection: 'row', alignItems: 'center'}}>
              <Checkbox
                status={
                  props.currentStore.stamps === 1 ? 'checked' : 'unchecked'
                }
                color={appColors.black}
                uncheckedColor={appColors.blue}
              />
              <Text style={{color: appColors.black, fontFamily: fonts.light}}>
                Η επιχείρηση υποστηρίζει stamps
              </Text>
            </View>

            <View style={{flex: 1, flexDirection: 'row', alignItems: 'center'}}>
              <Checkbox
                status={
                  props.currentStore.cashBack === 1 ? 'checked' : 'unchecked'
                }
                color={appColors.black}
                uncheckedColor={appColors.mainGray}
              />
                <Text style={{color: appColors.black, fontFamily: fonts.light}}>
                Η επιχείρηση υποστηρίζει cashBack
              </Text>
            </View>
          </View>
        </View>
      </ScrollView>
      {/*<View style={localStyles.column}>*/}
      {/*  <Pressable*/}
      {/*    disabled={props.currentStore.telephone === null}*/}
      {/*    onPress={() => handlePhoneCall(props.currentStore.telephone)}>*/}
      {/*    <Avatar.Icon*/}
      {/*      size={48}*/}
      {/*      icon="phone"*/}
      {/*      style={{*/}
      {/*        backgroundColor:*/}
      {/*          props.currentStore.telephone === null*/}
      {/*            ? 'gray'*/}
      {/*            : appColors.mainBlue,*/}
      {/*      }}*/}
      {/*      color={appColors.blue}*/}
      {/*    />*/}
      {/*  </Pressable>*/}
      {/*</View>*/}
      {/*<View style={localStyles.column}>*/}
      {/*  <Pressable*/}
      {/*    disabled={*/}
      {/*      props.currentStore.longitude === null ||*/}
      {/*      props.currentStore.latitude === null*/}
      {/*    }*/}
      {/*    onPress={() => {*/}
      {/*      handleLocation(*/}
      {/*        props.currentStore.longitude,*/}
      {/*        props.currentStore.latitude,*/}
      {/*      );*/}
      {/*    }}>*/}
      {/*    <Avatar.Icon*/}
      {/*      size={48}*/}
      {/*      icon="map-marker"*/}
      {/*      style={{*/}
      {/*        backgroundColor:*/}
      {/*          props.currentStore.longitude === null ||*/}
      {/*          props.currentStore.latitude === null*/}
      {/*            ? 'gray'*/}
      {/*            : appColors.mainBlue,*/}
      {/*      }}*/}
      {/*      color={appColors.blue}*/}
      {/*    />*/}
      {/*  </Pressable>*/}
      {/*</View>*/}
      {/*<View style={localStyles.column} />*/}
      {/*<View style={localStyles.column} />*/}
      {/*<View style={localStyles.column} />*/}
    </View>
  );
};

const localStyles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
    marginHorizontal: appWidth * 0.05,
    marginTop: appHeight * 0.02,
    justifyContent: 'space-between',
  },
  scheduleTitle: {
    textAlign: 'center',

    marginVertical: appHeight * 0.04,
    fontSize: 18,
      fontFamily: fonts.light,
    color: appColors.black,
  },
  textInput: {
    // marginHorizontal: appWidth * 0.05,
      marginLeft: -50,
    backgroundColor: 'transparent',
  },
  column: {flex: 0.2, flexDirection: 'column'},
});

export default StoreBottomSheet;
