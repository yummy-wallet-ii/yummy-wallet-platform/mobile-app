import React, {useEffect, useState} from 'react';
import {Image, ScrollView, StyleSheet, Text, View} from 'react-native';
import {appColors, appHeight, fonts} from '../../styles';
const StampsComponent = props => {
  const [initialStamps, setInitialStamps] = useState([]);
  let initializeStamps;
  useEffect(() => {
    const initializeStamps = () => {
      const tempStamps = [];
      for (let i = 0; i < props.limit; i++) {
        const slot = {
          id: i,
          hasStamp: i < props.stamps,
        };
        tempStamps.push(slot);
      }

      setInitialStamps(tempStamps);
    };
    initializeStamps();
  }, [initializeStamps, props]);

  const displayStamps = () => {
    return initialStamps.map(s => {
      return (
        <View style={localStyles.stampContainer} key={s.id}>
          {s.hasStamp ? (
            <Image
              style={localStyles.image}
              source={require('../../assets/images/defaultStamp.png')}
            />
          ) : null}
        </View>
      );
    });
  };
  return (
    <View style={{flexDirection: 'column'}}>
      <View style={{flexDirection: 'row', paddingHorizontal: 10}}>
        <Text style={{textAlign: 'center', fontFamily: fonts.bold}}>Στάμπες</Text>
      </View>

      <View>
        <ScrollView style={{height: appHeight * 0.5}}>
          <View style={{flexDirection: 'row', flexWrap: 'wrap', flex: 1}}>
            {displayStamps()}
          </View>
        </ScrollView>
      </View>
    </View>
  );
};

const localStyles = StyleSheet.create({
  stampContainer: {
    width: 42,
    height: 42,
    borderRadius: 10,
    borderColor: appColors.mainBlue,
    borderWidth: 1,
    justifyContent: 'center',
    alignItems: 'center',
    margin: 10,
  },
  image: {
    width: 42,
    height: 42,
    borderRadius: 10,
  },
});
export default StampsComponent;
