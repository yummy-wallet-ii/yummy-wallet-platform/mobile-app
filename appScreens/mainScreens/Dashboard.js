import React, {Component} from 'react';
import {StyleSheet} from 'react-native';

import {
  changeLoading,
  changeLoggedIn,
  errorHandling,
  setUser,
} from '../../storage/actions/actions';
import {appColors, appWidth} from '../../styles';
import AnimTab1 from '../../shared/BottomTab';
import {retrieveToken} from '../../services/commonService';
import {fetchUserInfo} from '../../services/userService';
import {connect} from "react-redux";

class Dashboard extends Component {
  state = {data: {}};

  constructor(props) {
    super(props);

    this.props.navigation.addListener('focus', async () => {
      await this.userValidation();
    });
  }

  navigateTo = route => {
    this.props.navigation.navigate(route);
  };

  async userValidation() {
    try {
      const token = await retrieveToken();
      if (!token) {
        this.props.changeLoggedIn(false);
      } else {
        const userInfo = await fetchUserInfo();
        this.props.setUser(userInfo);
        this.props.changeLoggedIn(true);
      }
    } catch (e) {
      this.props.changeLoggedIn(false);
    }
  }

  render() {
    return <AnimTab1 />;
  }
}

const mapDispatchToProps = dispatch => {
  return {
    changeLoading: value => dispatch(changeLoading(value)),
    errorHandling: value => dispatch(errorHandling(value)),
    changeLoggedIn: value => dispatch(changeLoggedIn(value)),
    setUser: value => dispatch(setUser(value)),
  };
};

export default connect(mapDispatchToProps)(Dashboard);

const localStyles = StyleSheet.create({
  bottom: {
    position: 'absolute',
    width: appWidth * 0.8,
    borderRadius: 15,
    left: appWidth * 0.1,
    bottom: 5,
    justifyContent: 'space-evenly',
    backgroundColor: appColors.mainBlue,
  },
});
