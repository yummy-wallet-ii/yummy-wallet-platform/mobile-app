import React, {Component} from 'react';
import {
  ImageBackground,
  SafeAreaView,
  ScrollView,
  Text,
  View,
} from 'react-native';
import {appColors, appHeight, styles} from '../../styles';
import {MainHeader} from '../../shared/MainHeader';
import {SearchBar} from '../../shared/SearchBar';
import {
  changeLoading,
  errorHandling,
  setStores,
} from '../../storage/actions/actions';
import {connect} from 'react-redux';
import {StoreCard} from './StoreCard';

const storesView = stores =>
  stores.map(item => {
    return (
      <View key={item.id} style={{marginBottom: appHeight * 0.01}}>
        <StoreCard store={item} />
      </View>
    );
  });
class SearchStores extends Component {
  state = {
    stores: [],
    storesLoaded: false,
    displayStores: [],
    searchValue: '',
  };
  constructor(props) {
    super(props);
  }

  handleCallback = value => {
    if (!value || value === '') {
      this.setState({displayStores: [...this.state.stores]});
    } else {
      const searchValue = value.toLowerCase();
      const originalStores = [...this.state.stores];
      const stores = originalStores.filter(store =>
        store.legalName.toLowerCase().includes(searchValue),
      );
      this.setState({displayStores: stores});
    }
  };

  componentDidMount() {
    try {
      this.props.changeLoading(true);
      console.log(`stores length ${this.props.actions.stores.length}`);
      this.setState({
        stores: this.props.actions.stores,
        displayStores: this.props.actions.stores,
        storesLoaded: true,
      });
    } catch (e) {
      this.props.errorHandling({visible: true, message: e.message});
    } finally {
      this.props.changeLoading(false);
    }
  }

  render() {
    return (
      <View style={styles.mainContainer}>
        <ImageBackground
          source={require('../../assets/images/backgrounds/mainBlack.png')}
          resizeMode="cover"
          style={styles.image}>
          <SearchBar parentCallback={this.handleCallback} />
          <ScrollView
            style={{
              flexDirection: 'column',
              marginTop: appHeight * 0.02,
              marginBottom: appHeight * 0.2,
            }}>
            <View
              style={{
                justifyContent: 'space-evenly',
                flexDirection: 'row',
                flexWrap: 'wrap',
              }}>
              {this.state.storesLoaded
                ? storesView(this.state.displayStores)
                : null}
            </View>
          </ScrollView>
        </ImageBackground>
      </View>
    );
  }
}

const mapStateToProps = state => {
  return {actions: state.actions};
};

const mapDispatchToProps = dispatch => {
  return {
    changeLoading: value => dispatch(changeLoading(value)),
    errorHandling: value => dispatch(errorHandling(value)),
    setStores: value => dispatch(setStores(value)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(SearchStores);
