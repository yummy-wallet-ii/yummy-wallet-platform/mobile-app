import React, {useEffect, useState} from 'react';
import {Image, Pressable, StyleSheet, Text, View} from 'react-native';
import {fetchCategoryImage} from '../../services/categoriesService';
import {appColors, fonts} from '../../styles';
import {useNavigation} from '@react-navigation/native';

export const CategoryCard = ({category, storePublicKey, storeId}) => {
  const [image, setImage] = useState('');
  const [loading, setLoading] = useState(true);

  const navigation = useNavigation();
  useEffect(() => {
    const fetchImage = async () => {
      const categoryImage = await fetchCategoryImage(category.id);
      setLoading(false);
      setImage(categoryImage);
    };

    if (loading) {
      fetchImage();
    }
  }, [category.id, loading]);

  const handlePress = () => {
    navigation.navigate('products', {
      category: category,
      storePublicKey,
      storeId,
    });
  };

  return (
    <Pressable
      onPress={handlePress}
      style={localStyles.cardContainer}
      key={category.id}>
      <View style={localStyles.cardImage}>
        {image === '' ? null : (
          <Image
            style={{
              borderRadius: 5,
              width: 100,
              height: 100,
            }}
            source={{uri: `data:image/png;base64, ${image}`}}
          />
        )}
        {category.hasStamps ? (
            <Image
                style={{
                  borderRadius: 5,
                  width: 24,
                  height: 24,
                  position: 'absolute',
                  bottom: 0,
                  right: 0
                }}
                source={require('../../assets/images/defaultStamp.png')}
            />
        ) : null}
      </View>

      <View style={{justifyContent: 'center'}}>
        <Text style={{color: appColors.mainWhite, fontFamily: fonts.light, textAlign: 'left', marginLeft: 0}}>
          {category.description}
        </Text>
      </View>
    </Pressable>
  );
};

const localStyles = StyleSheet.create({
  cardContainer: {
    width: 100,
    height: 100,
    borderRadius: 15,
    backgroundColor: 'white',
    marginVertical: 20,
    marginHorizontal: 10
  },
  cardImage: {},
});
