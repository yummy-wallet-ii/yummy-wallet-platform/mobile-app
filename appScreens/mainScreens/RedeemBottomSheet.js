import {Button, Text, TextInput} from "react-native-paper";
import {appColors, appHeight, appWidth, fonts} from "../../styles";
import React, {useState} from "react";
import {Alert, ScrollView, StyleSheet, View} from "react-native";
import {createRedeemRequest} from "../../services/redeemService";
import {useDispatch} from 'react-redux';
import {changeLoading, showMessage} from "../../storage/actions/actions";

export const RedeemBottomSheet = props => {
    const [amount, setAmount] = useState(0);
    const [requestMessage, setRequestMessage] = useState('');
    const dispatch = useDispatch();
    const showAlert = () =>
        Alert.alert(
            'Εξαργύρωση Yummy Coins',
            'Δημιουργίο αίτησης?',
            [
                {
                    text: 'Ναι',
                    onPress: () => redeemCoins(),
                    style: 'cancel',
                },
                {
                    text: 'Όχι',
                    style: 'cancel',
                },
            ],
            {
                cancelable: true,
            },
        );
    const redeemCoins = async () => {
        const body = {
            amount,
            status: 'PENDING'
        }
        try {
            dispatch(changeLoading(true));
            const response = await createRedeemRequest(body);
            setRequestMessage(response.message);
        } catch (e) {
            setRequestMessage(e.message);
            dispatch(showMessage({visible: true, message: e.message}));
        } finally {
            dispatch(changeLoading(false));
        }
    }

    return (<View style={localStyles.container}>
        <ScrollView style={{flex: 1}}>
            <View style={{flexDirection: 'column'}}>
                <Text style={{fontFamily: fonts.light, fontSize: 18}}>Συμπληρώστε το πλήθος των νομισμάτων που θα θέλατε να εξαργυρώσετε</Text>
                <TextInput
                    editable={true}
                    onChangeText={(text) => setAmount(text)}
                    style={localStyles.textInput}
                    theme={{colors: {text: appColors.black}}}
                    underlineColor={appColors.black}
                />
                <Button onPress={showAlert}>Εξαργύρωση</Button>
                <View>
                    <Text style={{fontFamily: fonts.bold}}>
                        {requestMessage}
                    </Text>
                </View>
            </View>
        </ScrollView>
    </View>)
}

const localStyles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'row',
        marginHorizontal: appWidth * 0.05,
        marginTop: appHeight * 0.02,
        justifyContent: 'space-between',
    },
    textInput: {
      marginTop: 15,
    },
    column: {flex: 0.2, flexDirection: 'column'},
});
