import React from 'react';
import {Text, View} from 'react-native';
import {appColors} from '../../styles';

export const NotLoggedInPage = () => {
  return (
    <View
      style={{
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
      }}>
      <View style={{flexDirection: 'row'}}>
        <Text style={{color: appColors.mainWhite}}>Δεν είστε συνδεδεμένος</Text>
      </View>

      <View style={{flexDirection: 'row'}}>
        <Text style={{color: appColors.mainWhite}}>
          Συνδεθείτε για να δείτε τις αγαπημένες σας επιλογές
        </Text>
      </View>
    </View>
  );
};
