import React, {Component} from "react";
import {changeLoading, errorHandling, setUser, showMessage} from "../../storage/actions/actions";
import {connect} from "react-redux";
import {fetchRedeemRequests} from "../../services/redeemService";
import {appColors, fonts, styles} from "../../styles";
import {SimpleBackHeader} from "../../shared/SimpleBackHeader";
import {ActivityIndicator, Image, ImageBackground, ScrollView, StyleSheet, Text, View} from "react-native";
import {OrderStyles} from "./Orders";
import app from "react-native/template/App";

class RedeemRequests extends Component {
    state = {
        requestsLoaded: false,
        requests: [],
        isLoading: true,
    };

    constructor(props) {
        super(props);
        this.props.navigation.addListener('focus', async () => {
            await this.fetchRequests();
        });
    }

    async componentDidMount() {
        await this.fetchRequests();
    }

    async fetchRequests() {
        try {
            const requests = await fetchRedeemRequests();
            console.log(requests);
            this.setState({requests});
        } catch (e) {
            this.props.showMessage({visible: true, message: {body: e.message}});
        } finally {
            this.setState({storesLoaded: true, isLoading: false});
        }
    }

    displayRequests() {
        return this.state.requests.map(request => {
            return (
                <View style={OrderStyles.row} key={request.id}>
                    <View style={OrderStyles.container}>
                        <View style={OrderStyles.insideRow}>
                            <Text style={OrderStyles.textPrefix}>Κωδικός αιτήματος</Text>
                            <Text style={OrderStyles.orderId}> {request.id}</Text>
                        </View>

                        <View style={OrderStyles.insideRow}>
                            <Text style={OrderStyles.textPrefix}>Yummy Coins</Text>
                            <Text style={OrderStyles.textValue}> {request.amount}</Text>
                        </View>

                        <View style={OrderStyles.insideRow}>
                            <Text style={OrderStyles.textPrefix}>Ημ.Αιτήματος</Text>
                            <Text style={OrderStyles.textValue}> {request.requestDate}</Text>
                        </View>

                        <View style={OrderStyles.insideRow}>
                            <Text style={OrderStyles.textPrefix}>Κατάσταση Αιτήματος</Text>
                            <Text style={[OrderStyles.textValue, request.status === 'PENDING' ? {color: appColors.orange} : request.status === 'CANCELLED' ? {color: appColors.mainRed} : {color: 'green'}]}> {request.status}</Text>
                        </View>

                        <View style={OrderStyles.insideRow}>
                            <Text style={OrderStyles.textPrefix}>Ημ.Ενέργειας</Text>
                            <Text style={OrderStyles.textValue}> {request.actionDate ? request.actionDate : '-'}</Text>
                        </View>
                    </View>
                </View>
            );
        })
    }

    render() {
        return (
            <View style={[styles.mainContainer, {flex: 1}]}>
                <ImageBackground
                    source={require('../../assets/images/backgrounds/mainBlack.png')}
                    resizeMode="cover"
                    style={styles.image}>
                    <SimpleBackHeader
                        title={'Αιτήματα εξαργύρωσης'}
                        isTransactions={false}
                    />
                    <View style={{flex: 1, backgroundColor: 'transparent'}}>
                        {this.state.requests.length === 0 ? (
                            <View
                                style={{
                                    flex: 1,
                                    justifyContent: 'center',
                                    alignItems: 'center',
                                    backgroundColor: 'transparent',
                                }}>
                                <Text style={{color: appColors.iceWhite, fontFamily: fonts.light}}>
                                    Δεν υπάρχουν αιτήματας
                                </Text>
                            </View>
                        ) : (
                            <View
                                style={{
                                    flex: 1,
                                    backgroundColor: 'transparent',
                                }}>
                                <ScrollView style={OrderStyles.scrollView}>
                                    {this.displayRequests()}
                                </ScrollView>
                            </View>
                        )}
                    </View>
                </ImageBackground>
            </View>
        )
    }
}

const mapStateToProps = state => {
    return {actions: state.actions};
};

const mapDispatchToProps = dispatch => {
    return {
        changeLoading: value => dispatch(changeLoading(value)),
        errorHandling: value => dispatch(errorHandling(value)),
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(RedeemRequests);
