import React, {Component} from 'react';
import {appColors, appHeight, appWidth, fonts, styles} from '../../styles';
import {BackHeader} from '../../shared/BackHeader';
import {ImageBackground, ScrollView, View, StyleSheet, Linking} from 'react-native';
import {
  changeLoading,
  changeLoggedIn,
  errorHandling,
  setCart,
  setOrder,
  setUser,
} from '../../storage/actions/actions';
import {connect} from 'react-redux';
import {Button} from 'react-native-paper';
import {
  addFavoriteStore,
  isFavoriteStore,
  removeFavoriteStore,
} from '../../services/storeService';
import RBSheet from 'react-native-raw-bottom-sheet';
import StoreBottomSheet from './StoreBottomSheet';
import {fetchStoreCategories} from '../../services/categoriesService';
import {CategoryCard} from './CategoryCard';
import StampsComponent from './StampsComponent';
import CouponsComponent from "./CouponsComponent";

class StoreDetails extends Component {
  state = {
    store: {},
    displayCouponSheet: false,
    displayStoreDetails: false,
    isFavorite: false,
    ephemeralKey: null,
    paymentIntent: null,
    categories: [],
  };

  constructor(props) {
    super(props);
  }

  async componentDidMount() {
    try {
      this.setState({store: this.props.route.params.store});
      if (this.props.actions.isLoggedIn) {
        const isFavorite = await isFavoriteStore(
          this.props.route.params.store.id,
        );
        this.setState({isFavorite});
      }

      await this.fetchCategories();
      this.initializeStoreCart();
    } catch (e) {
      this.props.errorHandling({visible: true, message: e.message});
    }
  }

  initializeStoreCart = () => {
    const cart = this.props.actions.cart;
    if (!cart[this.state.store.id]) {
      cart[this.state.store.id] = {
        publicKey: this.state.store.publicKey,
        orderItems: [],
      };
    }

    this.props.setCart(cart);
    // const orders = this.props.actions.orders;
    // const storeCart = orders.find(cart => cart.merchantId === store.merchantId);
    // if (!storeCart) {
    //   orders.push(store);
    //   this.props.setOrder(orders);
    // }
  };

  async fetchCategories() {
    let categories = await fetchStoreCategories(
      this.props.route.params.store.id,
    );

    categories.sort((a, b) => {
      return a.description.localeCompare(b.description);
    });
    this.setState({categories});
  }

  async addStoreToFavorites() {
    try {
      await addFavoriteStore(this.state.store.id);
      this.setState({isFavorite: true});
    } catch (e) {
      this.props.errorHandling({visible: true, message: e.message});
    }
  }

  async removeStoreFromFavorites() {
    try {
      await removeFavoriteStore(this.state.store.id);
      this.setState({isFavorite: false});
    } catch (e) {
      this.props.errorHandling({visible: true, message: e.message});
    }
  }

  displayCategories = () => {
    return this.state.categories.map(category => {
      return (
        <CategoryCard
          key={category.id}
          category={category}
          storePublicKey={this.props.route.params.store.publicKey}
          storeId={this.props.route.params.store.id}
        />
      );
    });
  };

  showCoupons = async () => {
    this.setState({displayCouponSheet: true, displayStoreDetails: false});
    this.RBSheet.open();
  };

  navigateToMaps = () => {
    const {latitude, longitude} = this.state.store;
    const latLng = `${latitude},${longitude}`;
    const url = `google.navigation:q=${latLng}`;
    Linking.canOpenURL(url).then(supported => {
      if (!supported) {
        console.log('Can\'t handle url: ' + url);
      } else {
        return Linking.openURL(url);
      }
    }).catch(err => console.error('An error occurred', err));
  }

  render() {
    return (
      <View style={styles.mainContainer}>
        <ImageBackground
          source={require('../../assets/images/backgrounds/mainBlack.png')}
          resizeMode="cover"
          style={styles.image}>
          <BackHeader
            title={this.state.store.legalName}
            storeId={this.state.store.id}
            publicKey={this.state.store.publicKey}
            hasCoupons={false}
            showCoupons={this.showCoupons}
          />

          <View style={localStyles.imageRow}>
            <ImageBackground
              source={
                this.state.store.frontImage === null
                  ? require('../../assets/images/store-default.png')
                  : {uri: `${this.state.store.frontImage}`}
              }
              style={{flex: 1}}
              resizeMode="cover"
            />
          </View>

          <View style={{alignItems: 'flex-end'}}>
            <Button
              onPress={() => {
                this.setState({
                  displayCouponSheet: false,
                  displayStoreDetails: true,
                });

                this.RBSheet.open();
              }}
              color={appColors.mainWhite}
              labelStyle={{color: appColors.iceWhite, fontFamily: fonts.italic}}
              icon="information-outline"
              contentStyle={{flexDirection: 'row-reverse'}}
              uppercase={false}>
              Πληροφορίες Καταστήματος
            </Button>

            <Button
                onPress={() => {
                this.navigateToMaps()
                }}
                color={appColors.mainWhite}
                labelStyle={{color: appColors.iceWhite, fontFamily: fonts.italic}}
                icon="information-outline"
                contentStyle={{flexDirection: 'row-reverse'}}
                uppercase={false}>
              Οδηγίες για το κατάστημα
            </Button>

            {this.props.actions.isLoggedIn ? (
              this.state.isFavorite ? (
                <Button
                  color={appColors.mainWhite}
                  icon="heart"
                  contentStyle={{flexDirection: 'row-reverse'}}
                  uppercase={false}
                  onPress={() => this.removeStoreFromFavorites()}>
                  Αφαίρεση απο τα αγαπημένα
                </Button>
              ) : (
                <Button
                  color={appColors.mainWhite}
                  icon="cards-heart-outline"
                  contentStyle={{flexDirection: 'row-reverse'}}
                  uppercase={false}
                  onPress={() => this.addStoreToFavorites()}>
                  Προσθήκη στα αγαπημένα
                </Button>
              )
            ) : null}
          </View>

          <ScrollView>
            <View
              style={{
                flexDirection: 'row',
                flexWrap: 'wrap',
                padding: 10,
                marginBottom: 10,
              }}>
              {this.displayCategories()}
            </View>
          </ScrollView>
        </ImageBackground>
        <RBSheet
          ref={ref => {
            this.RBSheet = ref;
          }}
          animationType={'slide'}
          height={appHeight * 0.60}
          closeOnDragDown={true}
          dragFromTopOnly={true}
          customStyles={{
            container: {borderRadius: 5},
          }}>
          {this.state.displayCouponSheet ? <CouponsComponent /> : null}

          {this.state.displayStoreDetails ? (
            <StoreBottomSheet currentStore={this.state.store} />
          ) : null}
        </RBSheet>
      </View>
    );
  }
}

const localStyles = StyleSheet.create({
  imageRow: {
    flexDirection: 'row',
    width: appWidth,
    height: appHeight * 0.25,
  },
  textInput: {
    marginHorizontal: appWidth * 0.05,
    backgroundColor: 'transparent',
  },
  scheduleContainer: {
    marginHorizontal: appWidth * 0.05,
    marginBottom: appHeight * 0.04,
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
  },
  item: {
    padding: 12,
    fontSize: 20,
    height: 47,
    borderBottomWidth: 1,
    borderBottomColor: appColors.mainWhite,
  },
  itemDescription: {
    color: appColors.mainWhite,
  },
  scheduleTitle: {
    textAlign: 'center',
    marginVertical: appHeight * 0.04,
    fontSize: 20,
    fontWeight: 700,
    color: appColors.mainBlue,
  },
  checkRow: {
    flexDirection: 'row',
    flex: 1,
    alignItems: 'center',
    marginLeft: appWidth * 0.05,
  },
  checkCol1: {
    flexDirection: 'column',
    flex: 0.1,
  },
  checkCol2: {flexDirection: 'column', flex: 0.9},
});

const mapStateToProps = state => {
  return {actions: state.actions};
};

const mapDispatchToProps = dispatch => {
  return {
    changeLoading: value => dispatch(changeLoading(value)),
    errorHandling: value => dispatch(errorHandling(value)),
    changeLoggedIn: value => dispatch(changeLoggedIn(value)),
    setUser: value => dispatch(setUser(value)),
    setOrder: value => dispatch(setOrder(value)),
    setCart: value => dispatch(setCart(value)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(StoreDetails);
