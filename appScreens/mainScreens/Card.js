import React, {useEffect, useState} from 'react';
import {
  Alert,
  ScrollView,
  StyleSheet,
  TextInput,
  Text,
  Button,
  View,
  TouchableOpacity,
} from 'react-native';
import {
  CardField,
  CardFieldInput,
  createPaymentMethod,
  initStripe,
  StripeProvider,
  useConfirmPayment,
} from '@stripe/stripe-react-native';
import {appColors, appHeight} from '../../styles';
import {getRequest, postRequest} from '../../services/commonService';
import {BackHeader} from '../../shared/BackHeader';
import QRCodeScanner from 'react-native-qrcode-scanner';
import {BarCodeReadEvent} from 'react-native-camera';
import BuyButton from './BuyButton';
import {useSelector} from 'react-redux';
import {useNavigation} from '@react-navigation/native';

export default function Card({route, navigation}) {
  const state = useSelector(state => state);
  const [name, setName] = useState('');
  const [isLoading, setIsLoading] = useState(false);
  const [displayScanner, setDisplayScanner] = useState(true);
  const [price, setPrice] = useState('0');
  const [shouldInitPayment, setShouldInitPayment] = useState(false);
  const [merchantId, setMerchantId] = useState(null);
  const [publicKey, setPublicKey] = useState('');
  const {confirmPayment, loading} = useConfirmPayment();

  useEffect(() => {
    async function initialize() {
      setPublicKey(route.params.publicKey);
      setMerchantId(route.params.merchantId);
      setName(state.actions.user.identifier);
      await initStripe({
        publishableKey: route.params.publicKey,
      });
    }
    initialize().catch(console.error);
  }, []);

  const fetchPaymentIntentClientSecret = async () => {
    const {clientSecret} = await postRequest(
      'transactions/init-payment-stripe',
      {
        merchantId,
        amount: price,
      },
    );

    return clientSecret;
  };

  const handlePayPress = async () => {
    try {
      setIsLoading(true);
      // 1. fetch Intent Client Secret from backend
      const clientSecret = await fetchPaymentIntentClientSecret();

      // 2. Gather customer billing information (ex. email)
      const billingDetails = {
        email: state.actions.user.identifier,
      };

      const {error, paymentIntent} = await confirmPayment(clientSecret, {
        paymentMethodType: 'Card',
        paymentMethodData: {billingDetails},
      });

      if (error) {
        Alert.alert(`Error code: ${error.code}`, error.message);
        console.log('Payment confirmation error', error.message);
        console.log(error);
      } else if (paymentIntent) {
        Alert.alert(
          'Success',
          `The payment for ${paymentIntent.amount} was confirmed successfully!`,
        );
        console.log('Success from promise', paymentIntent);
      }
    } catch (e) {
      console.log(e);
      Alert.alert(`Error at server ${e.error}`);
    } finally {
      setIsLoading(false);
    }
  };

  const onSuccess = e => {
    console.log(e.data);
  };

  const dataRead = function (data) {
    try {
      const actualData = JSON.parse(data.data);
      if (!actualData) {
        return;
      }
      if (actualData.client !== 'yummywallet') {
        return;
      }
      setPrice(actualData.price.toString());
      setDisplayScanner(false);
      setShouldInitPayment(true);
    } catch (e) {
      console.log(e);
    }

    // const values = actualData.split("&");
    // if(values.length !== 2) { return; }
    //
    // const clientString = values[0];
    // const priceString = values[1];
    //
    // const clientArray = clientString.split('=');
    // const client = clientArray[1];
    //
    // const priceArray = priceString.split('=');
    // const price = priceArray[1];
    //
    // if(client !== 'yummywallet') { return; }
    //
  };
  const navigateToTransactions = async () => {
    await navigation.navigate('transactions');
  };
  return (
    <ScrollView style={styles.mainContainer}>
      <BackHeader title="Πληρωμή" />

      {displayScanner ? (
        <View style={styles.qrContainer}>
          <QRCodeScanner
            cameraType={'back'}
            cameraProps={{
              onBarCodeRead(event: BarCodeReadEvent) {
                dataRead(event);
              },
            }}
            showMarker={true}
            fadeIn={true}
            onRead={onSuccess.bind(this)}
            containerStyle={{
              justifyContent: 'center',
              alignItem: 'center',
              alignContent: 'center',
            }}
            cameraStyle={{
              height: 200,
              width: 210,
              alignSelf: 'center',
              justifyContent: 'center',
            }}
          />
        </View>
      ) : null}

      <View style={{paddingHorizontal: 5}}>
        <TextInput
          autoCapitalize="none"
          placeholder="Τιμή"
          keyboardType="name-phone-pad"
          editable={false}
          value={price}
          onChange={value => setPrice(value.nativeEvent.text)}
          style={styles.input}
        />
        <TextInput
          autoCapitalize="none"
          placeholder="Ονοματεπώνυμο κατόχου"
          keyboardType="name-phone-pad"
          editable={false}
          value={name}
          onChange={value => setName(value.nativeEvent.text)}
          style={styles.input}
        />

        {shouldInitPayment ? (
          <BuyButton
            publicKey={publicKey}
            amount={price}
            merchantId={merchantId}
            user={state.actions.user}
          />
        ) : null}
      </View>
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  preview: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  capture: {
    flex: 0,
    backgroundColor: '#fff',
    borderRadius: 5,
    padding: 15,
    paddingHorizontal: 20,
    alignSelf: 'center',
    margin: 20,
  },
  centerText: {
    flex: 1,
    fontSize: 18,
    padding: 32,
    color: '#777',
  },
  textBold: {
    fontWeight: '500',
    color: '#000',
  },
  buttonText: {
    fontSize: 21,
    color: 'rgb(0,122,255)',
  },
  buttonTouchable: {
    padding: 16,
  },
  mainContainer: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: appColors.mainWhite,
  },
  container: {
    flex: 1,
    backgroundColor: appColors.mainBlue,
    paddingTop: 20,
    paddingHorizontal: 16,
  },
  cardField: {
    width: '100%',
    height: 50,
    marginVertical: 30,
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 20,
  },
  text: {
    color: appColors.mainBlue,
    marginLeft: 12,
  },
  input: {
    height: 44,
    borderBottomColor: 'gray',
    color: appColors.mainBlue,
    borderBottomWidth: 1.5,
  },
  qrContainer: {
    width: '100%',
    height: appHeight * 0.4,
    backgroundColor: appColors.mainBlue,
  },
});

const inputStyles: CardFieldInput.Styles = {
  borderWidth: 1,
  backgroundColor: '#FFFFFF',
  borderColor: '#000000',
  borderRadius: 8,
  fontSize: 14,
  placeholderColor: '#999999',
};
