import React, {useEffect, useState} from 'react';
import QRCodeScanner from 'react-native-qrcode-scanner';
import {BarCodeReadEvent} from 'react-native-camera';
import { Alert, ImageBackground, StyleSheet, Text, View } from "react-native";
import {fetchStoreById} from '../../services/storeService';
import {useDispatch, useSelector} from 'react-redux';
import {changeLoading} from '../../storage/actions/actions';
import {appColors, appHeight, styles} from '../../styles';
import {BackHeader} from '../../shared/BackHeader';
import { SimpleBackHeader } from "../../shared/SimpleBackHeader";

const ScanPage = props => {
  const state = useSelector(state => state);
  const dispatch = useDispatch();
  const [price, setPrice] = useState('0');
  const [merchantId, setMerchantId] = useState(null);
  const [publicKey, setPublicKey] = useState('');
  const onSuccess = e => {
    console.log(e.data);
  };

  const dataRead = async function (data) {
    try {
      const actualData = JSON.parse(data.data);
      console.log(actualData);
      if (!actualData) {
        return;
      }
      if (actualData.client !== 'yummywallet') {
        return;
      }
      // if (!actualData.merchantId) {
      //   return;
      // }
      dispatch(changeLoading(true));
      setPrice(actualData.price.toString());
      setMerchantId(actualData.merchantId);
      const store = await fetchStoreById(+actualData.merchantId);
      console.log(store);
      setPublicKey(store.publicKey);
      Alert.alert('got it');
    } catch (e) {
      console.log(e);
    } finally {
      dispatch(changeLoading(false));
    }
  };

  return (
    <View
      style={{
        flex: 1,
        flexDirection: 'column',
        backgroundColor: appColors.mainBlue,
      }}>
      <ImageBackground
        source={require('../../assets/images/backgrounds/mainBlack.png')}
        resizeMode="cover"
        style={styles.image}>
        <SimpleBackHeader title="Σάρωση κωδικού"/>
        <View style={localStyles.qrContainer}>
          <View style={{paddingHorizontal: 15}}>
            <Text style={{color: appColors.mainWhite, justifyContent: 'center'}}>Σαρώστε τον QR κωδικό από το κατάστημα και λάβετε την επιστροφή των χρημάτων σας</Text>
          </View>
          <QRCodeScanner
            cameraType={'back'}
            cameraProps={{
              onBarCodeRead(event: BarCodeReadEvent) {
                dataRead(event).then();
              },
            }}
            showMarker={true}
            fadeIn={true}
            onRead={onSuccess.bind(this)}
            containerStyle={{
              justifyContent: 'center',
              alignItem: 'center',
              alignContent: 'center',
            }}
            cameraStyle={{
              height: 200,
              width: 210,
              alignSelf: 'center',
            }}
          />
        </View>
      </ImageBackground>
    </View>
  );
};
const localStyles = StyleSheet.create({
  centerText: {
    flex: 1,
    fontSize: 18,
    color: '#777',
  },
  textBold: {
    fontWeight: '500',
    color: '#000',
  },
  buttonText: {
    fontSize: 21,
    color: 'rgb(0,122,255)',
  },
  buttonTouchable: {
  },
  mainContainer: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: appColors.mainWhite,
  },
  container: {
    flex: 1,
    backgroundColor: appColors.mainBlue,
    paddingHorizontal: 16,
  },
  cardField: {
    width: '100%',
    height: 50,
    marginVertical: 30,
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 20,
  },
  text: {
    color: appColors.mainBlue,
    marginLeft: 12,
  },
  input: {
    height: 44,
    borderBottomColor: 'gray',
    color: appColors.mainBlue,
    borderBottomWidth: 1.5,
  },
  qrContainer: {
    width: '100%',
    flex: 1,
    backgroundColor: appColors.transparent,
  },
});

export default ScanPage;
