import React, {Component} from 'react';
import {Image, ImageBackground, Text, View} from 'react-native';
import {appHeight, appWidth, styles} from '../../styles';
import {
  changeLoading,
  changeLoggedIn,
  errorHandling,
  setUser,
} from '../../storage/actions/actions';
import {connect} from 'react-redux';
import FavoritesPage from './FavoritesPage';
import {NotLoggedInPage} from './NotLoggedInPage';

class Favorites extends Component {
  state = {};
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <View style={styles.mainContainer}>
        <ImageBackground
          source={require('../../assets/images/backgrounds/mainBlack.png')}
          resizeMode="cover"
          style={styles.image}>
          {/*<MainHeader title="Αγαπημένα" />*/}

          {this.props.actions.isLoggedIn ? (
            <FavoritesPage {...this.props} />
          ) : (
            <NotLoggedInPage />
          )}
        </ImageBackground>
      </View>
    );
  }
}

const mapStateToProps = state => {
  return {actions: state.actions};
};

const mapDispatchToProps = dispatch => {
  return {
    changeLoading: value => dispatch(changeLoading(value)),
    errorHandling: value => dispatch(errorHandling(value)),
    changeLoggedIn: value => dispatch(changeLoggedIn(value)),
    setUser: value => dispatch(setUser(value)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Favorites);
