import React, {useEffect, useState} from 'react';
import {StripeProvider, usePaymentSheet} from '@stripe/stripe-react-native';
import {postRequest} from '../../services/commonService';
import {Alert, StyleSheet, View} from 'react-native';
import {IconButton} from 'react-native-paper';

const BuyButton = props => {
  const [ready, setReady] = useState(false);
  const {initPaymentSheet, presentPaymentSheet} = usePaymentSheet();
  useEffect(() => {
    console.log('buy button initialized');
    initializePaymentSheet();
  }, [initializePaymentSheet]);

  const fetchPaymentSheetParams = async () => {
    // console.log('requesting data');
    const body = {
      amount: props.amount,
      email: props.user.identifier,
      merchantId: props.merchantId,
    };
    // console.log(body);
    const {paymentIntent, ephemeralKey, customer} = await postRequest(
      'transactions/stripe',
      body,
    );
    // console.log('got data');
    return {
      paymentIntent,
      ephemeralKey,
      customer,
    };
  };

  const initializePaymentSheet = async () => {
    const {paymentIntent, ephemeralKey, customer} =
      await fetchPaymentSheetParams();

    const {error} = await initPaymentSheet({
      customerId: customer,
      customerEphemeralKeySecret: ephemeralKey,
      paymentIntentClientSecret: paymentIntent,
      merchantDisplayName: 'Burgers Example',
      allowsDelayedPaymentMethods: true,
      returnURL: 'stripe-example://stripe-redirect',
      // applePay: {
      //   merchantCountryCode: 'GR'
      // },
      googlePay: {
        merchantCountryCode: 'GR',
        testEnv: true,
        currencyCode: 'eur',
      },
    });

    if (error) {
      Alert.alert(`Error code: ${error.code}`, error.message);
    } else {
      setReady(true);
    }
  };

  async function buy() {
    const {error} = await presentPaymentSheet();
    if (error) {
      console.log(error);
    } else {
      props.proceedWithTransaction('done');
      // const params = {
      //   storeId: props.merchantId,
      //   customerId: props.user.id,
      //   price: props.amount,
      // };
      //
      // try {
      //   await createOrder(props.order);
      //   await createTransaction(params);
      //   clearCart();
      //   await navigateToTransactions();
      // } catch (e) {
      //   console.log(e);
      // }
    }
  }

  // function clearCart() {
  //   const cart = state.actions.cart;
  //   console.log(`Merchant Id while clearing - ${props.merchantId}`);
  //   const storeCart = cart[props.merchantId];
  //   storeCart.orderItems = [];
  //   dispatch(setCart(cart));
  // }

  return (
    <View>
      <StripeProvider publishableKey={props.publicKey}>
        <IconButton
          disabled={!ready}
          icon="credit-card-marker-outline"
          size={20}
          color={'green'}
          onPress={buy}
        />
      </StripeProvider>
    </View>
  );
};

const styles = StyleSheet.create({
  buttonText: {
    fontSize: 21,
    color: 'rgb(0,122,255)',
  },
  buttonTouchable: {
    padding: 16,
  },
});

export default BuyButton;
