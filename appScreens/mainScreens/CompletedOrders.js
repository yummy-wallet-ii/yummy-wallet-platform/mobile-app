import { ImageBackground, ScrollView, Text, View } from "react-native";
import { appColors, styles } from "../../styles";
import React, { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { changeLoading, showMessage } from "../../storage/actions/actions";
import { fetchCompletedOrders } from "../../services/ordersService";
import { OrderStyles } from "./Orders";
import { DisplayOrderComponent } from "./DisplayOrderComponent";

export const CompletedOrders = () => {
  const dispatch = useDispatch();
  const [orders, setOrders] = useState([]);

  useEffect(() => {
    fetchOrders();
  }, [])
  const fetchOrders = async () => {
    try {
      dispatch(changeLoading(true));
      const completed = await fetchCompletedOrders();
      setOrders(completed);
    } catch (e) {
      dispatch(showMessage({visible: true, message:{ body: e.message}}));
    } finally {
      dispatch(changeLoading(false));
    }
  }

  const displayOrders = () => {
    return orders.map(order => {
      return <DisplayOrderComponent order={order}></DisplayOrderComponent>
    })
  }

  return (
    <ImageBackground
      source={require('../../assets/images/backgrounds/mainBlack.png')}
      resizeMode="cover"
      style={styles.image}>
      {orders.length === 0 ? (
        <View
          style={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor: 'transparent',
          }}>
          <Text style={{color: appColors.mainWhite}}>Δεν υπάρχουν ολοκληρωμένες παραγγελίες</Text>
        </View>
      ) : (
        <View
          style={{
            flex: 1,
            backgroundColor: 'transparent',
          }}>
          <View style={{padding: 10, justifyContent: 'center', alignContent: 'center'}}>
            <Text style={{color: appColors.mainWhite, fontSize: 16, textAlign: 'center'}}>Ολοκληρωμένες</Text>
          </View>
          <ScrollView style={OrderStyles.scrollView}>
            {displayOrders()}
          </ScrollView>
        </View>
      )}
    </ImageBackground>
  );
};
