import React, {Component} from 'react';
import {Image, SafeAreaView, ScrollView, Text, View} from 'react-native';
import {
  changeLoading,
  changeLoggedIn,
  errorHandling,
  setUser,
  showMessage,
} from '../../storage/actions/actions';
import {connect} from 'react-redux';
import {fetchFavoriteStores} from '../../services/storeService';
import {appColors, appHeight} from '../../styles';
import {StoreCard} from './StoreCard';
const storesView = stores =>
  stores.map(item => {
    return (
      <View key={item.id}>
        <StoreCard store={item} />
      </View>
    );
  });
class FavoritesPage extends Component {
  state = {
    storesLoaded: false,
    stores: [],
    isLoading: true,
  };

  constructor(props) {
    super(props);
    this.props.navigation.addListener('focus', async () => {
      await this.fetchStores();
    });
  }

  async componentDidMount() {
    await this.fetchStores();
  }

  async fetchStores() {
    try {
      console.log('fetching stores');
      if(this.props.actions.isLoggedIn) {
        const stores = await fetchFavoriteStores();
        this.setState({stores});
      }
    } catch (e) {
      this.props.showMessage({visible: true, message: {body: e.message}});
    } finally {
      this.setState({storesLoaded: true, isLoading: false});
    }
  }

  render() {
    return (
      <SafeAreaView
        style={{
          flex: 1,
          flexDirection: 'column',
          backgroundColor: appColors.transparent,
        }}>
        <View style={{padding: 10, flexDirection: 'row'}}>
          <Text
            style={{
              fontSize: 24,
              color: appColors.mainWhite,
            }}>
            Εδώ θα βρείς τα αγαπημένα σου καταστήματα
          </Text>
        </View>

        {this.state.isLoading ? null : this.state.stores.length > 0 ? (
          // <View style={{flexDirection: 'row', flexWrap: 'wrap', flex: 1}}>
          //   <ScrollView>{storesView(this.state.stores)}</ScrollView>
          // </View>
          <ScrollView
            style={{
              flexDirection: 'column',
              marginTop: appHeight * 0.02,
              marginBottom: appHeight * 0.2,
            }}>
            <View
              style={{
                justifyContent: 'space-evenly',
                flexDirection: 'row',
                flexWrap: 'wrap',
              }}>
              {storesView(this.state.stores)}
            </View>
          </ScrollView>
        ) : (
          <View
            style={{
              flexDirection: 'row',
              flex: 0.7,
            }}>
            <View
              style={{
                flexDirection: 'column',
                justifyContent: 'center',
                alignItems: 'center',
                paddingHorizontal: 20,
              }}>
              <Image source={require('../../assets/images/no_fav.png')} />
              <Text style={{fontSize: 16, color: appColors.mainWhite}}>
                Δεν έχεις προσθέσει ακόμα κανένα κατάστημα στα αγαπημένα σου
              </Text>
            </View>
          </View>
        )}
      </SafeAreaView>
    );
  }
}
const mapStateToProps = state => {
  return {actions: state.actions};
};

const mapDispatchToProps = dispatch => {
  return {
    changeLoading: value => dispatch(changeLoading(value)),
    errorHandling: value => dispatch(errorHandling(value)),
    changeLoggedIn: value => dispatch(changeLoggedIn(value)),
    setUser: value => dispatch(setUser(value)),
    showMessage: value => showMessage(setUser(value)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(FavoritesPage);
