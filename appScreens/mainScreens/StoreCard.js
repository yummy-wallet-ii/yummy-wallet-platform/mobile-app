import React, {useEffect, useState} from 'react';
import {
    ActivityIndicator, Image,
    Pressable,
    StyleSheet,
    Text,
    View,
} from 'react-native';
import {appColors, fonts} from '../../styles';
import {fetchImage} from '../../services/storeService';
import {useNavigation} from '@react-navigation/native';

export const StoreCard = ({store}) => {
    const [isImageLoading, setIsImageLoading] = useState(false);
    const [imageLoaded, setImageLoaded] = useState(false);
    const [currentStore, setCurrentStore] = useState(store);
    const navigation = useNavigation();

    const navigateToDetails = () => {
        navigation.navigate('details', {store: currentStore});
    };

    useEffect(() => {
        const getStoreImage = async storeId => {
            try {
                setIsImageLoading(true);
                const image = await fetchImage(storeId);
                store.frontImage = image && image !== 'undefined' ? image : null;
                setCurrentStore(store);
            } catch (e) {
                store.frontImage = null;
                setCurrentStore(store);
            } finally {
                setIsImageLoading(false);
                setImageLoaded(true);
            }
        };

        if (!imageLoaded) {
            getStoreImage(store.id);
        }
    }, [imageLoaded]);

    return (
        <Pressable
            style={{
                marginVertical: 10,
                flex: 1,
                flexDirection: 'column',
            }}
            onPress={() => {
                navigateToDetails();
            }}>
            <View style={localStyles.card}>
                <View
                    style={{
                        flex: 1,
                        justifyContent: 'center',
                        flexDirection: 'column',
                    }}>
                    {isImageLoading ? (
                        <ActivityIndicator
                            animating={true}
                            color={appColors.mainWhite}
                            style={{height: 80, opacity: 1.0}}
                            size="large"
                        />
                    ) : (
                        currentStore.frontImage && currentStore.frontImage !== 'undefined' ? (
                            <Image
                                style={localStyles.imageBackground}
                                source={
                                    {
                                        uri: `${currentStore.frontImage}`
                                    }
                                }
                            />
                        ) :  <Image
                            style={[localStyles.imageBackground, {width: '100%',
                                height: undefined,
                                aspectRatio: 1}]}
                            source={
                                 require('../../assets/images/store-default.png')

                            }
                        />
                    )}
                </View>
            </View>
            <View style={{flexDirection: 'row', flex: 1}}>
                <View style={{flex: 1, flexDirection: 'column'}}>
                    <Text
                        style={{
                            textAlign: 'left',
                            marginLeft: 10,
                            color: appColors.pencil,
                            fontSize: 16,
                            fontFamily: fonts.light
                        }}>
                        {currentStore.legalName}
                    </Text>
                </View>
            </View>
        </Pressable>
    );
};
const localStyles = StyleSheet.create({
    card: {
        width: 125,
        marginHorizontal: 10,
        height: 125,
        elevation: 12,
        borderColor: 'transparent',
        borderWidth: 0,
        backgroundColor: appColors.iceWhite,
        flexDirection: 'row',
        flex: 1,
        borderRadius: 5,

    },
    imageBackground: {
        flex: 1,
        borderRadius: 5,
    },
});
