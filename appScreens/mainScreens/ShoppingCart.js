import React, {Component} from 'react';
import {
  changeLoading,
  changeLoggedIn,
  errorHandling,
  setCart,
  setOrder,
  setUser,
  showMessage,
} from '../../storage/actions/actions';
import {connect} from 'react-redux';
import {Image, ImageBackground, ScrollView, Text, View} from 'react-native';
import {appColors, appHeight, styles} from '../../styles';
import {
  Dialog,
  IconButton,
  Button,
  Portal,
  Paragraph,
  TextInput,
} from 'react-native-paper';
import {initStripe} from '@stripe/stripe-react-native';
import BuyButton from './BuyButton';
import {ShoppingCartBackHeader} from '../../shared/ShoppingCartBackHeader';
import {createOrder} from '../../services/ordersService';
import {createTransaction} from '../../services/transactionService';
import {fetchStampsByCategoryId} from "../../services/productsService";

class ShoppingCart extends Component {
  state = {
    orderItems: [],
    paymentPrice: 0,
    merchantId: null,
    publicKey: null,
    canPay: false,
    isStripeInitialized: false,
    showConfirm: false,
    confirmAnswer: false,
    visibleDialog: false,
    selectedItem: {},
    comments: '',
    finalOrder: {},
    stamps: []
  };

  componentDidMount() {
    // console.log(this.props.route.params);

    this.props.navigation.addListener('focus', async () => {
      this.initializeComponent();
    });

    this.props.navigation.addListener('blur', async () => {});
  }

  initializeComponent = async () => {
    const cart = this.props.cart;
    const storeCart = cart[this.props.route.params.storeId];
    for (const item of storeCart.orderItems) {
      const stamps = await this.fetchMyStampsForCategory(this.props.route.params.storeId, item.itemCategoryId);
      const itemCategoryStamps = stamps.find(s => s.categoryId === item.itemCategoryId);
      if(itemCategoryStamps) {
        if(itemCategoryStamps.stamps === 5) {
          itemCategoryStamps.stamps++;
          item.removeFromCharge = true;
        } else {
          itemCategoryStamps.stamps++;
        }
        this.setState({stamps});
      }
    }
    const orderItems = storeCart.orderItems;
    console.log(orderItems.map(item => item.removeFromCharge));

    if (orderItems.length > 0) {
      const merchantId = this.props.route.params.storeId;
      const publicKey = storeCart.publicKey;
      this.setState({orderItems, merchantId, publicKey}, async () => {
        this.calculatePaymentPrice();
        await this.initializeStripe();
      });
    }
  };

  async fetchMyStampsForCategory(storeId, categoryId) {
    try {
      const stamps = this.state.stamps;
      this.props.changeLoading(true);
      const cStamps = await fetchStampsByCategoryId(storeId, categoryId);
      if(!stamps.some(s => s.categoryId === categoryId)) {
        stamps.push({categoryId, stamps: cStamps.stamps});
        this.setState({stamps});
      }
      return stamps;
    } catch (e) {
      this.props.errorHandling({visible: true, message: e.message});
    } finally {
      this.props.changeLoading(false);
    }
  }

  initializeStripe = async () => {
    try {
      await initStripe({
        publishableKey: this.state.publicKey,
      });
      this.setState({isStripeInitialized: true});
    } catch (e) {
      this.props.errorHandling({visible: true, message: e.message});
    }
  };

  calculatePaymentPrice = () => {
    let price = 0;
    for (const item of this.state.orderItems) {
      if(!item.removeFromCharge) {
        price+= item.itemPrice;
      }
    }
    // const price = this.state.orderItems.reduce(
    //   (partialSum, a) => { if(!a.removeFromCharge) {partialSum + a.itemPrice}},
    //   0,
    // );

    const finalOrder = {storeId: this.state.merchantId, items: [], price: 0};
    const items = [];
    for (const orderItem of this.state.orderItems) {
      const item = {
        id: orderItem.itemId,
        name: orderItem.itemName,
        comments: orderItem.comments,
        categoryId: orderItem.itemCategoryId,
        price: orderItem.itemPrice,
      };
      items.push(item);
    }
    finalOrder.items = items;
    finalOrder.price = price;
    console.log(finalOrder);
    this.setState({paymentPrice: price, finalOrder});
  };

  displayConfirmDelete = response => {
    if (response) {
      this.setState({showConfirm: true});
    }
  };

  payment = () => {
    this.setState({canPay: true});
  };

  openDescriptionDialog = orderItem => {
    const item = this.state.orderItems.find(
      i => orderItem.itemTempId === i.itemTempId,
    );
    this.setState({
      visibleDialog: true,
      selectedItem: item,
      comments: item.comments,
    });
  };

  insertCommentToItem = () => {
    this.setState({isStripeInitialized: false});
    const orderItems = [...this.state.orderItems];
    const item = orderItems.find(
      i => this.state.selectedItem.itemTempId === i.itemTempId,
    );
    item.comments = this.state.comments;
    this.setState(
      {
        orderItems,
        comments: '',
        selectedItem: {},
        visibleDialog: false,
      },
      () => {
        this.calculatePaymentPrice();
        this.saveOrdersToStorage(orderItems);
        setTimeout(() => {
          this.setState({isStripeInitialized: true});
        }, 1000);
      },
    );
  };

  removeProductFromCart = itemTempId => {
    this.setState({isStripeInitialized: false});

    const item = this.state.orderItems.find(
      orderItem => orderItem.itemTempId === itemTempId,
    );
    const orderItems = this.state.orderItems.filter(
      item => item.itemTempId !== itemTempId,
    );

    this.setState({orderItems}, () => {
      this.calculatePaymentPrice();
    });

    this.saveOrdersToStorage(orderItems);

    this.displayMessage(
      `Το προϊόν ${item.itemName} αφαιρέθηκε από το καλάθι σας`,
    );

    if (this.state.orderItems.length > 0) {
      setTimeout(() => {
        this.setState({isStripeInitialized: true});
      }, 1000);
    }
  };

  saveOrdersToStorage = orderItems => {
    const cart = this.props.cart;
    const storeCart = cart[this.state.merchantId];
    storeCart.orderItems = orderItems;
    this.props.setCart(cart);
  };

  displayMessage = message => {
    this.props.showMessage({
      visible: true,
      body: message,
    });
  };

  displayProductsInCart = () => {
    return this.state.orderItems.map(orderItem => {
      return (
        <View
          style={{
            height: 50,
            width: '100%',
            backgroundColor: 'white',
            paddingRight: 10,
            marginVertical: 5,
          }}
          key={orderItem.itemTempId}>
          <View style={{flexDirection: 'row', alignItems: 'center'}}>
            <Image
              style={{
                borderRadius: 15,
                width: 50,
                resizeMode: 'contain',
                height: 50,
              }}
              source={{uri: `data:image/png;base64, ${orderItem.itemImage}`}}
            />

            <View style={{flex: 1}}>
              <Text style={{color: appColors.mainBlue}}>
                {orderItem.itemName}
              </Text>
            </View>

            <IconButton
              icon="message"
              size={20}
              color={orderItem.comments ? 'green' : 'gray'}
              dark={true}
              onPress={() => this.openDescriptionDialog(orderItem)}
            />
            <Text>{orderItem.itemPrice} €</Text>

            <IconButton
              icon="minus"
              size={20}
              iconColor="red"
              color={'red'}
              dark={true}
              onPress={() => this.removeProductFromCart(orderItem.itemTempId)}
            />
          </View>
        </View>
      );
    });
  };

  handle = response => {
    this.setState({showConfirm: false, confirmAnswer: response});

    const cart = this.props.cart;
    const storeCart = cart[this.state.merchantId];
    storeCart.orderItems = [];
    if (response) {
      this.setState({orderItems: []}, () => {
        this.props.setCart(cart);
      });
    }
  };

  navigateToTransactions = async () => {
    await this.props.navigation.navigate('transactions');
  };

  proceedWithTransaction = async () => {
    const params = {
      storeId: this.state.merchantId,
      customerId: this.props.user.id,
      price: this.state.paymentPrice,
      order: this.state.finalOrder,
    };

    try {
      this.props.changeLoading(true);
      await createOrder(this.state.finalOrder);
      await createTransaction(params);
      this.clearCart();
      await this.navigateToTransactions();
    } catch (e) {
      console.log(e);
    } finally {
      this.props.changeLoading(false);
    }
  };

  clearCart = () => {
    const cart = this.props.cart;
    console.log(`Merchant Id while clearing - ${this.state.merchantId}`);
    const storeCart = cart[this.state.merchantId];
    storeCart.orderItems = [];
    this.props.setCart(cart);
    this.setState({orderItems: []});
  };

  render() {
    return (
      <View style={styles.mainContainer}>
        <ImageBackground
          source={require('../../assets/images/backgrounds/mainBlack.png')}
          resizeMode="cover"
          style={styles.image}>
          <ShoppingCartBackHeader
            title={'Το Καλάθι σας'}
            storeId={this.state.merchantId}
            displayConfirmDelete={this.displayConfirmDelete}
          />

          {this.state.orderItems.length === 0 ? (
            <View
              style={{
                flex: 1,
                height: '100%',
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <IconButton
                color={appColors.mainWhite}
                icon={'cart-minus'}
                size={64}
              />
              <Text style={{color: appColors.mainWhite}}>
                Δεν υπάρχουν ακόμα προϊόντα στο καλάθι σας
              </Text>
            </View>
          ) : (
            <View style={{flex: 1}}>
              <View style={{height: appHeight * 0.85}}>
                <ScrollView>{this.displayProductsInCart()}</ScrollView>
              </View>

              <View style={{backgroundColor: 'white', flex: 1, width: '100%'}}>
                <View
                  style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    flex: 1,
                    paddingHorizontal: 10,
                  }}>
                  <Text
                    style={{
                      flex: 1,
                      fontWeight: '700',
                      fontSize: 18,
                      color: appColors.mainBlue,
                    }}>
                    Ποσό πληρωμής
                  </Text>
                  <Text style={{fontWeight: '700', color: appColors.mainBlue}}>
                    {this.state.paymentPrice} €
                  </Text>

                  {this.state.isStripeInitialized ? (
                    <BuyButton
                      order={this.state.finalOrder}
                      proceedWithTransaction={this.proceedWithTransaction}
                      publicKey={this.state.publicKey}
                      amount={this.state.paymentPrice}
                      merchantId={this.state.merchantId}
                      user={this.props.user}
                    />
                  ) : (
                    <IconButton
                      disabled={true}
                      icon="credit-card-marker-outline"
                      size={20}
                      color={'green'}
                    />
                  )}
                </View>
              </View>
            </View>
          )}

          <Portal>
            <Dialog
              visible={this.state.showConfirm}
              onDismiss={() => this.handle(false)}>
              <Dialog.Content>
                <Text>
                  Είστε σίγουροι ότι θέλετε να αδειάσετε το καλάθι σας?
                </Text>
              </Dialog.Content>
              <Dialog.Actions>
                <Button onPress={() => this.handle(false)} color={'red'}>
                  Όχι
                </Button>
                <Button onPress={() => this.handle(true)} color={'#1D8391'}>
                  Ναί
                </Button>
              </Dialog.Actions>
            </Dialog>
          </Portal>
        </ImageBackground>

        <Portal>
          <Dialog
            visible={this.state.visibleDialog}
            onDismiss={() => {
              this.setState({visibleDialog: false});
            }}>
            <Dialog.Content>
              <Paragraph> Εισάγετε σχόλια για το προϊόν</Paragraph>
              <TextInput
                value={this.state.comments}
                onChangeText={text => this.setState({comments: text})}
              />
            </Dialog.Content>
            <Dialog.Actions>
              <Button
                icon="check"
                mode="contained"
                onPress={() => this.insertCommentToItem()}>
                OK
              </Button>
            </Dialog.Actions>
          </Dialog>
        </Portal>
      </View>
    );
  }
}

const mapStateToProps = state => {
  const actions = state.actions;
  return {cart: actions.cart,
          user: actions.user};
};

const mapDispatchToProps = dispatch => {
  return {
    changeLoading: value => dispatch(changeLoading(value)),
    errorHandling: value => dispatch(errorHandling(value)),
    changeLoggedIn: value => dispatch(changeLoggedIn(value)),
    setUser: value => dispatch(setUser(value)),
    setOrder: value => dispatch(setOrder(value)),
    setCart: value => dispatch(setCart(value)),
    showMessage: value => dispatch(showMessage(value)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ShoppingCart);
