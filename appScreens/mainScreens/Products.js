import React, {Component} from 'react';
import {
  changeLoading,
  changeLoggedIn,
  errorHandling,
  setUser,
} from '../../storage/actions/actions';
import {connect} from 'react-redux';
import {appHeight, styles} from '../../styles';
import {BackHeader} from '../../shared/BackHeader';
import {ImageBackground, ScrollView, View} from 'react-native';
import {fetchProductsByCategoryId, fetchStampsByCategoryId} from '../../services/productsService';
import {ProductView} from './ProductView';
import RBSheet from 'react-native-raw-bottom-sheet';
import StampsComponent from './StampsComponent';

class Products extends Component {
  state = {
    category: null,
    storeId: -1,
    pageInitialized: false,
    products: [],
    storePublicKey: null,
    categoryStamps: 0,
    categoryStampsLimit: 0,
  };

  async componentDidMount() {
    const category = this.props.route.params.category;
    this.setState({
      category: this.props.route.params.category,
      categoryStampsLimit: category.stampsRewards,
      storeId: this.props.route.params.storeId,
      storePublicKey: this.props.route.params.storePublicKey,
      pageInitialized: true,
    });

    await this.fetchProducts(
      this.props.route.params.storeId,
      this.props.route.params.category.id,
    );

    await this.fetchMyStampsForCategory(
        this.props.route.params.storeId,
        this.props.route.params.category.id)
  }

  async fetchProducts(storeId, categoryId) {
    try {
      this.props.changeLoading(true);
      const products = await fetchProductsByCategoryId(storeId, categoryId);
      this.setState({products});
    } catch (e) {
      this.props.errorHandling({visible: true, message: e.message});
    } finally {
      this.props.changeLoading(false);
    }
  }

  async fetchMyStampsForCategory(storeId, categoryId) {
    try {
      this.props.changeLoading(true);
      const stamps = await fetchStampsByCategoryId(storeId, categoryId);
      this.setState({categoryStamps: stamps ? stamps.stamps : 0});
    } catch (e) {
      this.props.errorHandling({visible: true, message: e.message});
    } finally {
      this.props.changeLoading(false);
    }
  }

  showStamps = () => {
    this.RBSheet.open();
  };

  displayProducts() {
    return this.state.products.map(product => {
      return (
        <ProductView
          key={product.id}
          product={product}
          storePublicKey={this.state.storePublicKey}
          storeId={this.state.storeId}
        />
      );
    });
  }
  render() {
    return (
      <View style={styles.mainContainer}>
        <ImageBackground
          source={require('../../assets/images/backgrounds/mainBlack.png')}
          resizeMode="cover"
          style={styles.image}>
          <BackHeader
            title={
              this.state.pageInitialized ? this.state.category.description : ''
            }
            storeId={this.state.storeId}
            publicKey={this.state.storePublicKey}
            hasStatmps={this.props.route.params.category.hasStamps}
            stamps={5}
            showStamps={this.showStamps}
          />

          <ScrollView style={{padding: 5, margin: 5}}>
            {this.displayProducts()}
          </ScrollView>
        </ImageBackground>

        <RBSheet
          ref={ref => {
            this.RBSheet = ref;
          }}
          animationType={'slide'}
          height={appHeight * 0.25}
          minClosingHeight={5}
          closeOnDragDown={true}
          dragFromTopOnly={true}
          customStyles={{
            container: {borderRadius: 10},
          }}>
          <StampsComponent
            stamps={this.state.categoryStamps}
            limit={this.state.categoryStampsLimit}
          />
        </RBSheet>
      </View>
    );
  }
}

const mapStateToProps = state => {
  return {actions: state.actions};
};

const mapDispatchToProps = dispatch => {
  return {
    changeLoading: value => dispatch(changeLoading(value)),
    errorHandling: value => dispatch(errorHandling(value)),
    changeLoggedIn: value => dispatch(changeLoggedIn(value)),
    setUser: value => dispatch(setUser(value)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Products);
