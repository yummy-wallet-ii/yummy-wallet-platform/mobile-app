import React, {Component} from 'react';
import {
  changeLoading,
  changeLoggedIn,
  errorHandling,
} from '../../storage/actions/actions';
import {connect} from 'react-redux';
import {appHeight, appWidth, styles} from '../../styles';
import {ImageBackground, StyleSheet, View} from 'react-native';
import {BackHeader} from '../../shared/BackHeader';
import {SimpleBackHeader} from '../../shared/SimpleBackHeader';
import MapView, {
  MapCircle,
  MapMarker,
  PROVIDER_GOOGLE,
} from 'react-native-maps';

class StoresMap extends Component {
  state = {
    stores: [],
    mapIsLoaded: false,
    markers: [],
    region: {
      latitude: 40.593404,
      longitude: 22.951186,
      latitudeDelta: 0.015,
      longitudeDelta: 0.0121,
    },
    markerLocation: {
      latitude: 40.593042,
      longitude: 22.951227,
    },
  };
  constructor(props) {
    super(props);
  }

  async componentDidMount() {
    this.setState({stores: this.props.actions.stores});
    try {
    } catch (e) {
      console.log(e);
    }
  }

  onRegionChange(region) {
    this.setState({region});
  }

  displayMarkers() {
    return this.state.stores
      .filter(store => store.latitude !== null && store.longitude !== null)
      .map((store, index) => {
        if (store.latitude !== null && store.longitude !== null) {
          return (
            <MapMarker
              key={store.id}
              coordinate={{
                latitude: +store.latitude,
                longitude: +store.longitude,
              }}
              title={store.legalName}
              description={store.legalName}
            />
          );
        }
      });
  }

  displayCircles() {
    return this.state.stores
      .filter(store => store.latitude !== null && store.longitude !== null)
      .map((store, index) => {
        if (store.latitude !== null && store.longitude !== null) {
          return (
            <MapCircle
              key={store.id}
              fillColor={'yellow'}
              center={{latitude: +store.latitude, longitude: +store.longitude}}
              radius={100}
            />
          );
        }
      });
  }
  render() {
    return (
      <View style={styles.mainContainer}>
        <ImageBackground
          source={require('../../assets/images/backgrounds/mainBlack.png')}
          resizeMode="cover"
          style={styles.image}>
          <SimpleBackHeader title={'Χάρτης'} isTransactions={false} />
          <View style={localStyles.container}>
            <MapView
              showsMyLocationButton={true}
              showsUserLocation={true}
              onRegionChange={region => {
                this.onRegionChange(region);
              }}
              provider={PROVIDER_GOOGLE} // remove if not using Google Maps
              style={localStyles.map}
              initialRegion={this.state.region}>
              {/*<MapMarker*/}
              {/*  coordinate={this.state.markerLocation}*/}
              {/*  title={'Test Marker'}*/}
              {/*  description={'Test Description'}*/}
              {/*/>*/}
              {/*<MapCircle*/}
              {/*  fillColor={'yellow'}*/}
              {/*  title={'test'}*/}
              {/*  description={'test'}*/}
              {/*  center={this.state.markerLocation}*/}
              {/*  radius={100}*/}
              {/*/>*/}
              {this.displayMarkers()}
              {this.displayCircles()}
            </MapView>
          </View>
        </ImageBackground>
      </View>
    );
  }
}

const localStyles = StyleSheet.create({
  container: {
    ...StyleSheet.absoluteFillObject,
    height: appHeight,
    width: appWidth,
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  map: {
    ...StyleSheet.absoluteFillObject,
  },
});
const mapStateToProps = state => {
  return {actions: state.actions};
};

const mapDispatchToProps = dispatch => {
  return {
    changeLoading: value => dispatch(changeLoading(value)),
    errorHandling: value => dispatch(errorHandling(value)),
    changeLoggedIn: value => dispatch(changeLoggedIn(value)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(StoresMap);
