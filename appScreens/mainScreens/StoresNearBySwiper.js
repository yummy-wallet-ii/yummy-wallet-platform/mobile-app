import React from 'react';
import {ScrollView, View} from 'react-native';
import {StoreCard} from './StoreCard';

export const StoresNearBySwiper = ({stores}) => {
  const storesView = stores.map(item => {
    return (
      <View key={item.id}>
        <StoreCard store={item} />
      </View>
    );
  });

  return (
    <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
      {storesView}
    </ScrollView>
    // <Swiper
    //   loop={false}
    //   style={{overflow: 'visible'}}
    //   width={appWidth - 20}
    //   dotStyle={{backgroundColor: '#003087', opacity: 0.3}}
    //   activeDotStyle={{backgroundColor: '#003087'}}
    //   showsButtons={false}>
    //   {storesView}
    // </Swiper>
  );
};
