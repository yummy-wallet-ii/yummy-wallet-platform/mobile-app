import React, {Component} from 'react';
import {
  changeLoading,
  errorHandling,
  showMessage,
} from '../../storage/actions/actions';
import {connect} from 'react-redux';
import {appColors, fonts, styles} from '../../styles';
import {ImageBackground, StyleSheet, Text, View} from 'react-native';
import {SimpleBackHeader} from '../../shared/SimpleBackHeader';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {Button} from 'react-native-paper';
import {PendingOrders} from './PendingOrders';
import {CompletedOrders} from './CompletedOrders';
import MaterialCommunityIcon from 'react-native-paper/src/components/MaterialCommunityIcon';
const Tab = createBottomTabNavigator();

class Orders extends Component {
  state = {
    orders: [],
  };
  constructor() {
    super();
  }

  componentDidMount() {}

  fetchPendingOrders = async () => {};
  fetchCompletedOrders = async () => {};

  render() {
    return (
      <View style={[styles.mainContainer, {flex: 1}]}>
        <ImageBackground
          source={require('../../assets/images/backgrounds/mainBlack.png')}
          resizeMode="cover"
          style={styles.image}>
          <SimpleBackHeader
            title={'Οι παραγγελίες μου'}
            isTransactions={false}
          />
          <View style={{flex: 1, backgroundColor: 'transparent'}}>
            <Tab.Navigator
              screenOptions={({route}) => ({
                headerShown: false,
                tabBarShowLabel: false,
                tabBarIcon: ({focused, color, size}) => {
                  let iconName;

                  if (route.name === 'Pending') {
                    iconName = focused ? 'timer-sand-full' : 'timer-sand-empty';
                  } else if (route.name === 'Completed') {
                    iconName = focused ? 'check-bold' : 'check-outline';
                  }

                  // You can return any component that you like here!
                  return (
                    <MaterialCommunityIcon
                      name={iconName}
                      direction={'ltr'}
                      size={20}
                      color={appColors.mainBlue}
                      dark={false}
                      icon={iconName}
                      uppercase={false}
                    />
                  );
                },
                tabBarActiveTintColor: 'tomato',
                tabBarInactiveTintColor: 'gray',
              })}>
              <Tab.Screen name="Pending" component={PendingOrders} />
              <Tab.Screen name="Completed" component={CompletedOrders} />
            </Tab.Navigator>
          </View>
        </ImageBackground>
      </View>
    );
  }
}

export const OrderStyles = StyleSheet.create({
  scrollView: {
    padding: 10,
    marginBottom: 10,
  },
  row: {
    display: 'flex',
    width: '100%',
    backgroundColor: 'white',
    paddingRight: 10,
    marginVertical: 5,
    flexDirection: 'row',
    borderRadius: 5,
    padding: 10,
  },
  container: {
    flexDirection: 'column',
    flex: 0.8,
  },
  imageContainer: {
    flexDirection: 'column',
    flex: 0.2,
    justifyContent: 'center',
    alignContent: 'center',
  },
  imageBackground: {
    width: 60,
    height: 60,
    borderRadius: 80 / 2,
  },
  insideRow: {
    flexDirection: 'row',
  },
  textPrefix: {
    fontSize: 16,
    color: appColors.mainGray,
    fontFamily: fonts.light
  },
  orderId: {
    fontSize: 16,
    color: appColors.mainGray,
    fontFamily: fonts.bold,
    fontWeight: '600',
    textDecorationLine: 'underline',
  },
  textValue: {
    fontSize: 16,
    color: appColors.mainGray,
    fontFamily: fonts.light
  },
});

const mapStateToProps = state => {
  return {actions: state.actions};
};

const mapDispatchToProps = dispatch => {
  return {
    changeLoading: value => dispatch(changeLoading(value)),
    errorHandling: value => dispatch(errorHandling(value)),
    showMessage: value => dispatch(showMessage(value)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Orders);
