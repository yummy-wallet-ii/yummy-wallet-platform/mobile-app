import { ActivityIndicator, Image, Text, View } from "react-native";
import { OrderStyles } from "./Orders";
import React, { useEffect, useState } from "react";
import { fetchImage } from "../../services/storeService";
import { appColors } from "../../styles";

export const DisplayOrderComponent = ({order}) => {
  const [imageIsLoading, setImageIsLoading] = useState(true);
  const [storeImage, setStoreImage] = useState('');
  useEffect(() => {
    fetchStoreImage(order.storeId);
  }, []);

  const fetchStoreImage = async storeId => {
    try {
      setImageIsLoading(true);
      const image = await fetchImage(storeId);
      setStoreImage(image === '' ? null : image);
    } catch (e) {
      console.log(e);
    } finally {
      setImageIsLoading(false);
    }
  };
  return(
    <View style={OrderStyles.row} key={order.orderUniqueId}>
      <View style={OrderStyles.container}>
        <View style={OrderStyles.insideRow}>
          <Text style={OrderStyles.textPrefix}>Κωδικός παραγγελίας:</Text>
          <Text style={OrderStyles.orderId}> {order.orderUniqueId}</Text>
        </View>

        <View style={OrderStyles.insideRow}>
          <Text style={OrderStyles.textPrefix}>Κατάστημα:</Text>
          <Text style={OrderStyles.textValue}> {order.storeName}</Text>
        </View>

        <View style={OrderStyles.insideRow}>
          <Text style={OrderStyles.textPrefix}>Ημ.Παραγγελίας:</Text>
          <Text style={OrderStyles.textValue}> {order.orderDate}</Text>
        </View>

        <View style={OrderStyles.insideRow}>
          <Text style={OrderStyles.textPrefix}>Τιμή:</Text>
          <Text style={OrderStyles.textValue}> {order.price} €</Text>
        </View>
      </View>
      <View style={OrderStyles.imageContainer}>
        {imageIsLoading ? (
          <ActivityIndicator
            animating={true}
            color={appColors.mainWhite}
            style={{ height: 80, opacity: 1.0 }}
            size="large"
          />
        ) : (
          <Image
            style={OrderStyles.imageBackground}
            source={
              storeImage === null || storeImage === 'undefined'
                ? require('../../assets/images/store-default.png')
                : { uri: `${storeImage}` }
            }
          />)
        }
      </View>
    </View>
  )
}
