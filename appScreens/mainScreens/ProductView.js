import React, {useEffect, useState} from 'react';

import {
  ActivityIndicator,
  Image,
  Pressable,
  StyleSheet,
  Text,
  View,
} from 'react-native';
import {appColors, appHeight, fonts} from '../../styles';
import {fetchProductImage} from '../../services/productsService';
import RBSheet from 'react-native-raw-bottom-sheet';
import ProductDetails from './ProductDetails';

export const ProductView = ({product, storePublicKey, storeId}) => {
  const [image, setImage] = useState('');
  const [loading, setLoading] = useState(true);
  const [bottomSheet, setBottomSheet] = useState();

  useEffect(() => {
    const fetchImage = async () => {
      const productImage = await fetchProductImage(product.id);
      product.imageBlob = productImage;
      setLoading(false);
      setImage(productImage);
    };

    if (loading) {
      fetchImage();
    }
  }, [loading, product, product.id]);

  const displayBottomSheet = () => {
    bottomSheet.open();
  };

  return (
    <View style={localStyles.container}>
      <Pressable
        style={{
          flexDirection: 'row',
          flex: 0.8,
        }}
        onPress={displayBottomSheet}
        key={product.id}>
        {image === '' || loading ? (
          <View
            style={{flex: 1, justifyContent: 'center', alignContent: 'center'}}>
            <ActivityIndicator
              animating={true}
              color={appColors.mainGray}
              style={{height: 80, opacity: 1.0, alignSelf: 'center'}}
              size="large"
            />
          </View>
        ) : (
          <Image
            style={{
              // borderRadius: 15,
              width: '100%',
              resizeMode: 'contain',
              height: '100%',
            }}
            source={{uri: `data:image/png;base64, ${image}`}}
          />
        )}
      </Pressable>

      <View style={{flexDirection: 'row', flex: 0.2, alignItems: 'center'}}>
        <Text
          style={{
            fontSize: 18,
            flex: 1,
            color: appColors.black,
              fontFamily: fonts.bold,
            fontWeight: '600',
          }}>
          {product.title}
        </Text>

        {product.hasDiscount ? (
          <View>
            <Text
              style={{
                fontSize: 14,
                color: 'red',
                textDecorationLine: 'line-through',
              }}>
              {product.price} €
            </Text>

            <Text style={{fontSize: 14, color: appColors.mainBlue}}>
              {product.alternativePrice} €
            </Text>
          </View>
        ) : (
          <Text style={{fontSize: 14, color: appColors.mainBlue}}>
            {product.price} €
          </Text>
        )}
      </View>
      <RBSheet
        ref={ref => {
          setBottomSheet(ref);
        }}
        animationType={'fade'}
        height={appHeight * 0.6}
        closeOnDragDown={true}
        dragFromTopOnly={true}
        customStyles={{
          container: {borderRadius: 10},
        }}>
        <ProductDetails
          product={product}
          storePublicKey={storePublicKey}
          storeId={storeId}
          bottomSheetRef={bottomSheet}
        />
      </RBSheet>
    </View>
  );
};

const localStyles = StyleSheet.create({
  container: {
    width: '100%',
    padding: 5,
    flex: 1,
    borderRadius: 5,
    height: appHeight * 0.3,
    backgroundColor: appColors.mainWhite,
    marginVertical: 5,
    flexDirection: 'column',
  },
});
