import React from 'react';
import {View} from 'react-native';
import {styles} from '../../styles';
import {BackHeader} from '../../shared/BackHeader';

export const Help = () => {
  return (
    <View style={styles.mainContainer}>
      <BackHeader title="Βοήθεια" />
    </View>
  );
};
