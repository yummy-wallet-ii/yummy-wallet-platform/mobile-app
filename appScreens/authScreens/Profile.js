import React, {Component} from 'react';
import {
  Alert,
  FlatList,
  Image,
  ImageBackground,
  Linking,
  Pressable,
  StyleSheet,
  Text,
  View,
} from 'react-native';
import {appColors, appHeight, appWidth, fonts, styles} from '../../styles';
import {
  changeLoading,
  changeLoggedIn,
  errorHandling,
} from '../../storage/actions/actions';
import {connect} from 'react-redux';
import {fetchUserCredits, userLogout} from '../../services/userService';
import {Button} from 'react-native-paper';
import {useNavigation} from '@react-navigation/native';
import FlipCard from 'react-native-flip-card';
import {clearFCMToken} from '../../services/pushNotificationService';
import MaterialCommunityIcon from "react-native-paper/src/components/MaterialCommunityIcon";
import RBSheet from "react-native-raw-bottom-sheet";
import {RedeemBottomSheet} from "../mainScreens/RedeemBottomSheet";
const Item = ({item}) => {
  const navigation = useNavigation();

  const navigateTo = async item => {
    if (!item.link) {
      navigation.navigate(item.route);
    } else {
      const url = 'wallet://';
      try {
        const supported = await Linking.canOpenURL(url);
        if (supported) {
          await Linking.openURL(url);
        } else {
          Alert.alert(`Don't know how to open this URL: ${url}`);
        }
      } catch (e) {
        Alert.alert(e.message);
      }
    }
  };
  return (
    <Pressable onPress={() => navigateTo(item)} style={[localStyles.listRow, {justifyContent: 'center', alignItems: 'center'}]}>
      <View style={{marginRight: 15}}>
        <MaterialCommunityIcon color={appColors.mainWhite} size={16} name={item.icon}/>
      </View>
      <View style={{flex: 1, justifyContent: 'center'}}>
        <Text style={[localStyles.listTitle]}>{item.title}</Text>
      </View>
      <View>
        <MaterialCommunityIcon color={appColors.mainWhite} name="chevron-right" size={16}/>
      </View>
    </Pressable>
  );
};

class Profile extends Component {
  state = {
    myCode: '',
    user: null,
    credits: 0,
    listItems: [
      {
        id: 1,
        route: 'transactions',
        title: 'Οι συναλλαγές μου',
        link: null,
        icon: 'credit-card-outline',
      },
      {
        id: 4,
        route: 'orders',
        title: 'Οι παραγγελίες μου',
        link: null,
        icon: 'arrow-left-right',
      },
      {
        id: 2,
        route: 'wallet',
        title: 'Το πορτοφόλι μου',
        link: 'wallet',
        icon: 'wallet',
      },
      {
        id: 3,
        route: 'qrCode',
        title: ' O QR κωδικός μου',
        link: null,
        icon: 'qrcode-scan',
      },
    ],
  };

  constructor(props) {
    super(props);
    this.props.navigation.addListener('focus', async () => {
      if(this.props.actions.isLoggedIn) {

        let credits = await fetchUserCredits();
        credits = credits ? credits.credits : 0;
        this.setState({credits});
      }
    });
  }
  async componentDidMount() {
    try {
      this.props.changeLoading(true);
      if(this.props.actions.isLoggedIn) {
        const myCode = `https://chart.googleapis.com/chart?chs=250x250&cht=qr&chl=yummyWalletUserId=${this.props.actions.user.id}`;
        let credits = await fetchUserCredits();
        credits = credits ? credits.credits : 0;
        this.setState({myCode, user: this.props.actions.user, credits});
      }
    } catch (e) {

    } finally {
      this.props.changeLoading(false);
    }
  }

  navigateTo = async path => {
         this.props.navigation.navigate(path);
  };

  async logout() {
    try {
      await userLogout();
      await clearFCMToken();
      // this.props.navigation.navigate('external-logout');
      this.props.changeLoggedIn(false);
    } catch (e) {
      this.props.errorHandling({visible: true, message: e.message});
    }
  }

  navigateToRedeemPage() {
    this.props.navigation.navigate('dashboard');
  }

  showAlert = () =>
      Alert.alert(
          'Εξαργύρωση Yummy Coins',
          'Θέλετε να δημιουργήσετε μια αίτηση εξαργύρωσης?',
          [
            {
              text: 'Ναι',
              onPress: () =>     this.RBSheet.open(),
              style: 'cancel',
            },
            {
              text: 'Όχι',
              style: 'cancel',
            },
          ],
          {
            cancelable: true,
          },
      );

  renderItem = ({item}) => <Item item={item} />;

  render() {
    return (
      <View style={styles.mainContainer}>
        <View style={localStyles.creditCardContainer}>
          {/*<View style={localStyles.creditCard}>*/}

          <FlipCard flipHorizontal={true} flipVertical={false}>
            <View style={localStyles.face}>
              <ImageBackground
                style={localStyles.creditCard}
                resizeMode="cover"
                imageStyle={{borderRadius: 15}}
                source={require('../../assets/images/profileCardFront.png')}>
                <View
                  style={[
                    localStyles.cardRow,
                    {justifyContent: 'center', alignItems: 'center'},
                  ]}>
                  <Text style={[localStyles.cardText, {fontSize: 18}]}>
                    Yummy Wallet Loyalty Card
                  </Text>
                </View>

                <View style={[localStyles.cardRow, {marginTop: 20}]}>
                  <Text
                    style={[
                      localStyles.cardText,
                      {fontSize: 16, letterSpacing: 2},
                    ]}>
                    {this.state.user?.creditCardNumber}
                  </Text>
                </View>

                <View style={[localStyles.cardRow, {marginTop: 10, marginBottom: 40}]}>
                  <Text style={localStyles.cardText}>
                    {this.state.user?.firstName} {this.state.user?.lastName}
                  </Text>
                </View>
              </ImageBackground>
            </View>

            <View style={localStyles.back}>
              <ImageBackground
                style={localStyles.creditCard}
                resizeMode="cover"
                imageStyle={{borderRadius: 15}}
                source={require('../../assets/images/profileCardBack.png')}>
                <View
                  style={[
                    localStyles.cardRow,
                    {justifyContent: 'center', alignItems: 'center'},
                  ]}>
                  <View
                    style={{
                      alignItems: 'center',
                      justifyContent: 'center',
                      width: appWidth * 0.55,
                      height: appHeight * 0.17,
                    }}>
                    <Image
                      onLoadEnd={() => {
                        this.setState({isLoaded: true});
                      }}
                      resizeMode="cover"
                      source={{uri: this.state.myCode}}
                      style={{
                        width: 100,
                        height: 100,
                        borderRadius: 15,
                      }}
                    />
                  </View>
                </View>
              </ImageBackground>
            </View>
            {/*</View>*/}
          </FlipCard>
        </View>

        <View style={localStyles.listContainer}>
          <FlatList
            data={this.state.listItems}
            renderItem={this.renderItem}
            keyExtractor={item => item.id}
          />
        </View>

        <View style={{paddingHorizontal: 5}}>
          <Pressable style={localStyles.listRow}>
            <View style={{marginRight: 15}}>
              <MaterialCommunityIcon color={appColors.mainWhite} size={16} name="credit-card-outline"/>
            </View>

            <View
                onTouchStart={this.showAlert}
              style={{
                flex: 1,
                flexDirection: 'row',
                justifyContent: 'flex-start',
                alignItems: 'center',
              }}>
              <Text style={[localStyles.listTitle, {flex: 1}]}>
                Τα Yummy Coins μου
              </Text>
              <Text style={[localStyles.listTitle]}>{this.state.credits}</Text>
            </View>
          </Pressable>

          <Pressable onPress={() => this.navigateTo('redeem-requests')} style={[localStyles.listRow, {justifyContent: 'center', alignItems: 'center'}]}>
            <View style={{marginRight: 15}}>
              <MaterialCommunityIcon color={appColors.iceWhite} size={16} name={'credit-card-outline'}/>
            </View>
            <View style={{flex: 1, justifyContent: 'center'}}>
              <Text style={[localStyles.listTitle]}>{'Αιτήματα εξαργύρωσης'}</Text>
            </View>
            <View>
              <MaterialCommunityIcon color={appColors.mainWhite} name="chevron-right" size={16}/>
            </View>
          </Pressable>
        </View>
        {/*<View style={localStyles.listContainer2}>*/}
        {/*  <Text style={localStyles.credits }>*/}
        {/*    Τα credits μου: {this.state.credits}*/}
        {/*  </Text>*/}
        {/*</View>*/}

        <View
          style={{
            marginTop: appHeight * 0.06,
            flexDirection: 'row',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Pressable onPress={() => this.logout()} style={localStyles.button}>
            <Text style={localStyles.buttonText}>Αποσύνδεση</Text>
          </Pressable>
        </View>

        <RBSheet
            ref={ref => {
              this.RBSheet = ref;
            }}
            animationType={'slide'}
            height={appHeight * 0.40}
            closeOnDragDown={true}
            dragFromTopOnly={true}
            customStyles={{
              container: {borderRadius: 5},
            }}>
         <RedeemBottomSheet></RedeemBottomSheet>
        </RBSheet>
      </View>
    );
  }
}

const localStyles = StyleSheet.create({
  face: {},
  back: {},
  listContainer: {
    // flex: 0.6,
    marginTop: appHeight * 0.06,
    paddingHorizontal: 5,
  },

  credits: {
    color: 'white',
    fontSize: 22,
  },
  button: {
    width: appWidth * 0.4,
    borderRadius: 10,
    // borderWidth: 0.5,
    borderColor: appColors.mainWhite,
    justifyContent: 'center',
    alignItems: 'center',
    padding: 15,
  },
  buttonText: {
    fontSize: 16,
    color: appColors.mainWhite,
    textDecorationLine: 'underline',
    fontFamily: fonts.light
  },
  qrCodeContainer: {
    marginTop: (8.88 * appHeight) / 100,
    height: (50 * appHeight) / 100,
    width: appWidth,
    alignContent: 'center',
    justifyContent: 'center',
    alignItems: 'center',
  },

  creditCardContainer: {
    width: appWidth,
    height: appHeight * 0.2,
    backgroundColor: appColors.transparent,
    justifyContent: 'center',
    alignItems: 'center',
  },
  creditCard: {
    flexDirection: 'column',
    borderRadius: 15,
    padding: 15,
  },
  cardRow: {
    flexDirection: 'row',
  },
  cardText: {
    color: appColors.mainWhite,
  },
  qrCodeImage: {
    height: (50 * appWidth) / 100,
    width: (50 * appHeight) / 100,
  },
  listRow: {
    flexDirection: 'row',
    justifyContent: 'center',
    paddingLeft: 15,
    paddingRight: 15,
    marginVertical: 10,
    // borderBottomWidth: 1,
    // borderBottomColor: 'white',
  },
  listTitle: {
    fontSize: 18,
    color: appColors.iceWhite,
    fontFamily: fonts.light
  },
});

const mapStateToProps = state => {
  return {actions: state.actions};
};

const mapDispatchToProps = dispatch => {
  return {
    changeLoading: value => dispatch(changeLoading(value)),
    errorHandling: value => dispatch(errorHandling(value)),
    changeLoggedIn: value => dispatch(changeLoggedIn(value)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Profile);
