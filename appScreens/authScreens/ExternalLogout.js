import {StyleSheet} from 'react-native';
import React, {Component} from 'react';
import {WebView} from 'react-native-webview';
import {
  changeLoading,
  changeLoggedIn,
  errorHandling,
} from '../../storage/actions/actions';
import {connect} from 'react-redux';
const environment = {
  apiUrl: 'https://api1.uowm.gr/api/',
  logOutSSOUrl: 'https://sso.uowm.gr/logout',
  logOutUrl:
    'https://oauth.uowm.gr/auth/realms/universis/protocol/openid-connect/logout?GLO=true',
  externalLogout:
    'https://oauth.uowm.gr/auth/realms/universis/protocol/openid-connect/logout?GLO=true&redirect_uri=https://sso.uowm.gr/logout',
  usersUrl:
    'https://sso.uowm.gr/login?service=https%3A%2F%2Fidp.uowm.gr%2Fcasauth%2Ffacade%2Fnorenew%3Fidp%3Dhttps%3A%2F%2Fidp.uowm.gr%2Fidp%2FexternalAuthnCallback',
  studentsUrl: 'https://students2.uowm.gr',
  apiUrlDemo: 'https://api.universis.io/api/',
  usersDemoUrl: 'https://users.universis.io',
  studentsDemoUrl: 'https://students.universis.io',
  clientId: '6476063258140244',
  scope: 'students',
};
// const logoutOutUrl = `${environment.usersUrl}/logout?continue=${environment.studentsUrl}`;
const logoutOutUrl = `${environment.logOutUrl}`;
const logoutOutSSOUrl = `${environment.logOutSSOUrl}`;

class ExternalLogout extends Component {
  constructor(props) {
    super(props);
  }

  state = {
    url: logoutOutUrl,
    ssoUrl: logoutOutSSOUrl,
    gotData: false,
  };

  componentDidMount() {
    this.props.changeLoading(true);
    setTimeout(() => {
      this.setState({url: this.state.ssoUrl});
      this.props.changeLoading(false);
    }, 3000);
  }

  onLoad = async (webViewState: {url: string}) => {
    if (webViewState.url.startsWith(`${environment.logOutSSOUrl}`)) {
      if (!this.state.gotData) {
        try {
          setTimeout(async () => {
            console.log('logged user off');
            this.props.changeLoggedIn(false);
            this.props.navigation.goBack();
          }, 500);
        } catch (e) {
          console.log('====================Error====================');
          console.log(e);
          console.log('====================Error====================');
        }
      }
    }
  };

  render() {
    return (
      <WebView
        originWhitelist={['*']}
        style={styles.webview}
        onNavigationStateChange={webViewState => this.onLoad(webViewState)}
        source={{uri: this.state.url}}
      />
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  webview: {
    alignSelf: 'stretch',
    flex: 1,
  },
  video: {
    marginTop: 20,
    maxHeight: 200,
    width: 320,
    flex: 1,
  },
});

const mapStateToProps = state => {
  return {actions: state.actions};
};

const mapDispatchToProps = dispatch => {
  return {
    changeLoading: value => dispatch(changeLoading(value)),
    setError: value => dispatch(errorHandling(value)),
    changeLoggedIn: value => dispatch(changeLoggedIn(value)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ExternalLogout);
