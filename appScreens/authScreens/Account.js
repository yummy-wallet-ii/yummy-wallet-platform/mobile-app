import React, {Component} from 'react';
import {Image, ImageBackground, StyleSheet, Text, View} from 'react-native';
import {appHeight, appWidth, styles} from '../../styles';
import {MainHeader} from '../../shared/MainHeader';
import {
  changeLoading,
  changeLoggedIn,
  errorHandling,
  setStores,
  setUser,
} from '../../storage/actions/actions';
import {connect} from 'react-redux';
import Profile from './Profile';
import Login from './Login';

class Account extends Component {
  state = {
    myCode: '',
  };

  constructor(props) {
    super(props);
  }

  componentDidMount() {}

  render() {
    return (
      <View style={styles.mainContainer}>
        <ImageBackground
          source={require('../../assets/images/backgrounds/mainBlack.png')}
          resizeMode="cover"
          style={styles.image}>
          <MainHeader title="Ο Λογαριασμός μου" />

          {this.props.isLoggedIn ? (
            <Profile {...this.props} />
          ) : (
            <Login {...this.props} />
          )}
        </ImageBackground>
      </View>
    );
  }
}

const localStyles = StyleSheet.create({
  qrCodeContainer: {
    marginTop: (8.88 * appHeight) / 100,
    height: (50 * appHeight) / 100,
    width: appWidth,
    alignContent: 'center',
    justifyContent: 'center',
    alignItems: 'center',
  },
  qrCodeImage: {
    height: (50 * appWidth) / 100,
    width: (50 * appHeight) / 100,
  },
});

const mapStateToProps = state => {
  return {isLoggedIn: state.actions.isLoggedIn};
};
// const mapStateToProps = (state) => {
//   const { actions } = state;
//   const {isLoggedIn} = actions;
//   return {
//     isLoggedIn
//   };
// };

const mapDispatchToProps = dispatch => {
  return {
    changeLoading: value => dispatch(changeLoading(value)),
    errorHandling: value => dispatch(errorHandling(value)),
    changeLoggedIn: value => dispatch(changeLoggedIn(value)),
    setUser: value => dispatch(setUser(value)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Account);
