import React, {Component} from 'react';
import {
  Image,
  Pressable,
  StyleSheet,
  Text,
  TextInput,
  View,
} from 'react-native';
import {appColors, appHeight, appWidth, fonts, styles} from '../../styles';
import {
  changeLoading,
  changeLoggedIn,
  errorHandling,
  isBottomVisible,
  setUser,
  showMessage,
} from '../../storage/actions/actions';
import {connect} from 'react-redux';
import {fetchUserInfo, userLogin} from '../../services/userService';
import {storeToken} from '../../services/commonService';
import {getFCMToken} from '../../services/pushNotificationService';
import {Button} from 'react-native-paper';

class Login extends Component {
  state = {
    myCode: '',
    username: '',
    password: '',
  };

  constructor(props) {
    super(props);
  }

  async componentDidMount() {}
  async login() {
    try {
      this.props.changeLoading(true);
      const fcmToken = await getFCMToken();
      const user = {
        identifier: this.state.username,
        password: this.state.password,
        fcmToken,
      };

      const token = await userLogin(user);
      await storeToken(token);
      const userInfo = await fetchUserInfo();
      console.log(userInfo);
      this.props.setUser(userInfo);
      this.props.changeLoggedIn(true);
    } catch (e) {
      this.props.showMessage({visible: true, message: {body: e.message}});
    } finally {
      this.props.changeLoading(false);
    }
  }

  navigateToRegister = () => {
    this.props.navigation.navigate('register');
  };

  navigateToExternalLogin = () => {
    this.props.navigation.navigate('external-login', {service: 'login'});
  };

  render() {
    return (
      <View style={localStyles.mainContainer}>
        <View style={localStyles.formContainer}>
          <View>
            <View
              style={[
                localStyles.formRow,
                {
                  justifyContent: 'center',
                  alignItems: 'center',
                  marginBottom: 5,
                },
              ]}>
              <Text style={localStyles.headerText}>Συνδεθείτε</Text>
            </View>

            <View
              style={[
                localStyles.formRow,
                {
                  marginTop: 15,
                },
              ]}>
              <Text style={localStyles.label}>Όνομα χρήστη</Text>
            </View>

            <View style={[localStyles.formRow]}>
              <TextInput
                mode="flat"
                returnKeyType={'next'}
                style={localStyles.textInput}
                value={this.state.username}
                onChangeText={text => this.setState({username: text})}
              />
            </View>

            <View
              style={[
                localStyles.formRow,
                {
                  marginTop: 15,
                },
              ]}>
              <Text style={localStyles.label}>Κωδικός χρήστη</Text>
            </View>

            <View style={[localStyles.formRow]}>
              <TextInput
                mode="flat"
                returnKeyType={'done'}
                style={localStyles.textInput}
                secureTextEntry={true}
                value={this.state.password}
                onChangeText={text => this.setState({password: text})}
              />
            </View>

            <View
              style={[
                localStyles.formRow,
                {justifyContent: 'center', alignItems: 'center', marginTop: 20},
              ]}>
              <Button
                style={{alignItems: 'center'}}
                icon="login-variant"
                color={'white'}
                mode="contained-tonal"
                uppercase={false}
                onPress={() => {
                  this.login();
                }}>
                Σύνδεση
              </Button>

              {/*<Button*/}
              {/*  style={{alignItems: 'center'}}*/}
              {/*  icon="login-variant"*/}
              {/*  color={'white'}*/}
              {/*  mode="contained-tonal"*/}
              {/*  uppercase={false}*/}
              {/*  onPress={() => {*/}
              {/*    this.navigateToExternalLogin();*/}
              {/*  }}>*/}
              {/*  Σύνδεση*/}
              {/*</Button>*/}
            </View>

            <View
              style={[
                localStyles.formRow,
                {justifyContent: 'center', alignItems: 'center', marginTop: 20},
              ]}>
              <Pressable onPress={() => this.navigateToRegister()}>
                <Text style={localStyles.loginButtonText}>
                  Δεν έχετε λογαριασμό? Εγγραφείτε τώρα
                </Text>
              </Pressable>
            </View>
          </View>
        </View>
      </View>
    );
  }
}

const localStyles = StyleSheet.create({
  mainContainer: {
    position: 'relative',
    backgroundColor: appColors.transparent,
    width: appWidth,
    height: appHeight,
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  formContainer: {
    // elevation: 5,
    flex: 0.8,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'column',
    width: appWidth * 0.9,
    height: appHeight * 0.65,
    // padding: 15,
    backgroundColor: 'transparent',
    borderRadius: 30,
  },
  formRow: {
    marginVertical: 0,
    alignItems: 'flex-start',
    flexDirection: 'row',
  },
  headerText: {
    fontSize: 22,
    color: appColors.iceWhite,
    fontFamily: fonts.italic,
  },
  label: {
    fontSize: 16,
    color: appColors.iceWhite,
    fontFamily: fonts.light,
    marginBottom: 0,
  },
  textInput: {
    flex: 1,
    alignSelf: 'stretch',
    color: appColors.mainWhite,
    fontFamily: fonts.light,
    borderBottomColor: appColors.mainWhite,
    borderBottomWidth: 1, // Add this to specify bottom border thickness
  },
  loginButton: {
    borderRadius: 10,
    borderColor: appColors.mainWhite,
    borderWidth: 0.5,
    padding: 15,
    width: appWidth * 0.4,
    justifyContent: 'center',
    alignItems: 'center',
  },
  loginButtonText: {
    color: appColors.iceWhite,
    fontFamily: fonts.light,
  },
});

const mapStateToProps = state => {
  return {actions: state.actions};
};

const mapDispatchToProps = dispatch => {
  return {
    changeLoading: value => dispatch(changeLoading(value)),
    errorHandling: value => dispatch(errorHandling(value)),
    changeLoggedIn: value => dispatch(changeLoggedIn(value)),
    setUser: value => dispatch(setUser(value)),
    showMessage: value => dispatch(showMessage(value)),
    isBottomVisible: value => dispatch(isBottomVisible(value)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Login);
