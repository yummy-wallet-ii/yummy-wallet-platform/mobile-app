import React, {Component} from 'react';
import {
  ImageBackground,
  Pressable,
  ScrollView,
  StyleSheet,
  Text,
  TextInput,
  View,
} from 'react-native';
import {appColors, appHeight, appWidth, styles} from '../../styles';
import {BackHeader} from '../../shared/BackHeader';
import {
  changeLoading,
  errorHandling,
  setStores,
} from '../../storage/actions/actions';
import {connect} from 'react-redux';
import {userRegistration} from '../../services/userService';
import {Button} from 'react-native-paper';

class Register extends Component {
  state = {
    email: '',
    password: '',
    firstName: '',
    lastName: '',
    mobile: '',
    iban: '',
    isExternal: false,
  };
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    try {
      const importedData = this.props.route.params?.info;
      console.log(this.props.route.params.info);
      if (importedData) {
        const email = importedData.email;
        const firstName = importedData.firstName;
        const lastName = importedData.lastName;
        const mobile = importedData.phone;

        this.setState({email, firstName, lastName, mobile, isExternal: true});
      }
    } catch (e) {
      console.log(e);
    }
  }

  navigateBack = () => {
    this.props.navigation.goBack();
  };

  register = async () => {
    try {
      this.props.changeLoading(true);
      const user = {
        identifier: this.state.email,
        password: this.state.password,
        firstName: this.state.firstName,
        lastName: this.state.lastName,
        mobile: this.state.mobile,
        iban: this.state.iban,
        provider: 'local',
      };
      if (!this.isFormValid(user)) {
        this.props.errorHandling({
          visible: true,
          message: 'Παρακαλούμε συμπληρώστε όλα τα πεδία',
        });
        return;
      }

      await userRegistration(user);
      this.props.errorHandling({
        visible: true,
        message:
          'Η δημιουργία του λογαριασμού σας ολοκληρώθηκε με επιτυχία. Παρακαλούμε Συνδεθείτε',
      });
      this.props.navigation.goBack();
      // this.props.navigation.navigate('external-login', {service: 'login'});
    } catch (e) {
      this.props.errorHandling({visible: true, message: e.message});
    } finally {
      this.props.changeLoading(false);
    }
  };

  isFormValid = user => {
    if(this.state.isExternal) {
      return true;
    } else {
      return Object.values(user).every(x => x !== null && x !== '');
    }
  };

  render() {
    return (
      <View style={styles.mainContainer}>
        <ImageBackground
          source={require('../../assets/images/backgrounds/mainBlack.png')}
          resizeMode="cover"
          style={styles.image}>
          <BackHeader title="Εγγραφή" />

          <ScrollView
            style={localStyles.mainContainer}
            contentContainerStyle={{
              flex: 1,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <View style={localStyles.formContainer}>
              <View>
                <View style={[localStyles.formRow]}>
                  <Text style={localStyles.label}>Email</Text>
                </View>

                <View style={[localStyles.formRow]}>
                  <TextInput
                    mode="flat"
                    returnKeyType={'next'}
                    style={localStyles.textInput}
                    value={this.state.email}
                    editable={!this.state.isExternal}
                    onChangeText={text => this.setState({email: text})}
                  />
                </View>

                {!this.state.isExternal ? (
                    <>
                        <View
                            style={[
                              localStyles.formRow,
                              {
                                marginTop: 10,
                              },
                            ]}>
                          <Text style={localStyles.label}>Κωδικός</Text>
                        </View>

                    <View style={[localStyles.formRow]}>
                      <TextInput
                          mode="flat"
                          returnKeyType={'done'}
                          style={localStyles.textInput}
                          secureTextEntry={true}
                          value={this.state.password}
                          onChangeText={text => this.setState({password: text})}
                      />
                    </View>
                    </>
                ) : null}


                <View
                  style={[
                    localStyles.formRow,
                    {
                      marginTop: 10,
                    },
                  ]}>
                  <Text style={localStyles.label}>Όνομα</Text>
                </View>

                <View style={[localStyles.formRow]}>
                  <TextInput
                    mode="flat"
                    returnKeyType={'next'}
                    style={localStyles.textInput}
                    value={this.state.firstName}
                    editable={!this.state.isExternal}
                    onChangeText={text => this.setState({firstName: text})}
                  />
                </View>

                <View
                  style={[
                    localStyles.formRow,
                    {
                      marginTop: 10,
                    },
                  ]}>
                  <Text style={localStyles.label}>Επώνυμο</Text>
                </View>

                <View style={[localStyles.formRow]}>
                  <TextInput
                    mode="flat"
                    returnKeyType={'next'}
                    style={localStyles.textInput}
                    value={this.state.lastName}
                    onChangeText={text => this.setState({lastName: text})}
                  />
                </View>

                <View
                  style={[
                    localStyles.formRow,
                    {
                      marginTop: 10,
                    },
                  ]}>
                  <Text style={localStyles.label}>Αριθμός κινητού</Text>
                </View>

                <View style={[localStyles.formRow]}>
                  <TextInput
                    mode="flat"
                    returnKeyType={'next'}
                    style={localStyles.textInput}
                    value={this.state.mobile}
                    onChangeText={text => this.setState({mobile: text})}
                  />
                </View>

                <View
                  style={[
                    localStyles.formRow,
                    {
                      marginTop: 10,
                    },
                  ]}>
                  <Text style={localStyles.label}>IBAN</Text>
                </View>

                <View style={[localStyles.formRow]}>
                  <TextInput
                    mode="flat"
                    returnKeyType={'next'}
                    style={localStyles.textInput}
                    value={this.state.iban}
                    onChangeText={text => this.setState({iban: text})}
                  />
                </View>

                <View
                  style={[
                    localStyles.formRow,
                    {
                      justifyContent: 'center',
                      alignItems: 'center',
                      marginTop: 20,
                    },
                  ]}>
                  <Button
                    style={{alignItems: 'center'}}
                    icon="account-plus-outline"
                    color={'white'}
                    mode="contained-tonal"
                    uppercase={false}
                    onPress={() => {
                      this.register();
                    }}>
                    Εγγραφή
                  </Button>
                  {/*<Pressable*/}
                  {/*  style={localStyles.loginButton}*/}
                  {/*  onPress={() => this.register()}>*/}
                  {/*  <Text style={localStyles.loginButtonText}>Εγγραφή</Text>*/}
                  {/*</Pressable>*/}
                </View>
              </View>
            </View>
          </ScrollView>
        </ImageBackground>
      </View>
    );
  }
}
const localStyles = StyleSheet.create({
  mainContainer: {
    backgroundColor: appColors.transparent,
  },
  formContainer: {
    width: appWidth,
    padding: 15,
    flexDirection: 'column',
    backgroundColor: 'transparent',
  },
  formRow: {
    marginVertical: 0,
    flexDirection: 'row',
  },
  headerText: {
    fontSize: 22,
    color: appColors.mainWhite,
  },
  label: {
    fontSize: 16,
    color: appColors.mainWhite,
    marginBottom: 0,
  },
  textInput: {
    flex: 1,
    // alignSelf: 'stretch',
    marginTop: -5,
    color: appColors.mainWhite,
    borderBottomColor: appColors.mainWhite,
    borderBottomWidth: 1, // Add this to specify bottom border thickness
  },
  loginButton: {
    borderRadius: 10,
    borderColor: appColors.mainWhite,
    borderWidth: 0.5,
    padding: 15,
    width: appWidth * 0.4,
    justifyContent: 'center',
    alignItems: 'center',
  },
  loginButtonText: {
    color: appColors.mainWhite,
  },
});

const mapStateToProps = state => {
  return {actions: state.actions};
};

const mapDispatchToProps = dispatch => {
  return {
    changeLoading: value => dispatch(changeLoading(value)),
    errorHandling: value => dispatch(errorHandling(value)),
    setStores: value => dispatch(setStores(value)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Register);
