import React, {Component, useState} from 'react';
import {Text, View} from 'react-native';
import {appColors, appHeight, appWidth, fonts, styles} from '../../styles';
import LottieView from 'lottie-react-native';
import {ProgressBar} from 'react-native-paper';

export class SplashScreen extends Component {
  state = {
    progress: 0,
    loadingMessage: '',
  };
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    try {
      this.getStores();
      this.getData();
      this.getInfo();
      setTimeout(() => {
        // this.props.navigation.navigate('');
        this.props.navigation.navigate('dashboard');
      }, 6000);
    } catch (e) {
      console.log(e);
    }
  }

  getStores = () => {
    this.setState({loadingMessage: 'Καταστήματα'});
    setTimeout(() => {
      this.setState({loadingMessage: 'Λήψη καταστημάτων'});
    }, 2000);
  };
  getData = () => {
    this.setState({loadingMessage: 'Loading Data'});
    setTimeout(() => {
      this.setState({loadingMessage: 'Λήψη λοιπών πληροφοριών'});
    }, 4000);
  };
  getInfo = () => {
    this.setState({loadingMessage: ''});
    setTimeout(() => {
      this.setState({loadingMessage: 'Όλα είναι έτοιμα'});
    }, 5000);
  };

  render() {
    return (
      <View style={styles.mainContainer}>
        <View style={{flexDirection: 'row', flex: 0.6, justifyContent: 'center', alignItems: 'center'}}>
          <LottieView
            source={require('../../animatedIcons/logo.json')}
            style={{ width: appWidth/2, height: appHeight/4}}
            autoPlay={true}
            loop={false}
          />
          {/*  <LottieView*/}
          {/*      source={require('../../animatedIcons/logo.json')}*/}
          {/*      style={{ width: 100, height: 100 }}*/}
          {/*      autoPlay*/}
          {/*      loop*/}
          {/*  />*/}
        </View>
        <View
          style={{
            flexDirection: 'row',
            flex: 0.4,
          }}>
          <View
            style={{
              flexDirection: 'column',
              alignItems: 'flex-end',
              justifyContent: 'flex-end',
            }}>
            <View
              style={{
                flexDirection: 'row',
                alignSelf: 'center',
                marginBottom: 20,
              }}>
              <Text style={{fontSize: 18, fontFamily: fonts.light}}>{this.state.loadingMessage}</Text>
            </View>
            <View
              style={{
                flexDirection: 'row',
                justifySelf: 'flex-end',
                alignSelf: 'flex-end',
              }}>
              <ProgressBar
                style={{
                  width: appWidth,
                  height: 15,
                  backgroundColor: 'transparent',
                }}
                // progress={this.state.progress}
                indeterminate={true}
                color={appColors.secondaryBlue}
              />
            </View>
          </View>
        </View>
      </View>
    );
  }
}
