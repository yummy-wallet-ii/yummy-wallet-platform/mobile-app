import {Platform, StyleSheet} from 'react-native';
import React, {Component} from 'react';
import {WebView} from 'react-native-webview';
import {
  changeLoading,
  changeLoggedIn,
  errorHandling,
  setUser,
  showMessage,
} from '../../storage/actions/actions';
import {connect} from 'react-redux';
import {
  fetchUniversisInfo,
  fetchUserByEmail,
  fetchUserInfo,
  userExternalLogin,
  userLogin,
} from '../../services/userService';
import {storeToken} from '../../services/commonService';
import {getFCMToken} from '../../services/pushNotificationService';

const environment = {
  apiUrl: 'https://api1.uowm.gr/api/',
  logOutSSOUrl: 'https://sso.uowm.gr/logout',
  logOutUrl:
    'https://oauth.uowm.gr/auth/realms/universis/protocol/openid-connect/logout?GLO=true',
  externalLogout:
    'https://oauth.uowm.gr/auth/realms/universis/protocol/openid-connect/logout?GLO=true&redirect_uri=https://sso.uowm.gr/logout',
  usersUrl:
    'https://sso.uowm.gr/login?service=https%3A%2F%2Fidp.uowm.gr%2Fcasauth%2Ffacade%2Fnorenew%3Fidp%3Dhttps%3A%2F%2Fidp.uowm.gr%2Fidp%2FexternalAuthnCallback',
  studentsUrl: 'https://students2.uowm.gr',
  apiUrlDemo: 'https://api.universis.io/api/',
  usersDemoUrl: 'https://users.universis.io',
  studentsDemoUrl: 'https://students.universis.io',
  clientId: '6476063258140244',
  scope: 'students',
};

const link = `${environment.usersDemoUrl}/login?response_type=token&redirect_uri=${environment.studentsDemoUrl}/auth/callback/index.html&client_id=${environment.clientId}&scope=${environment.scope}`;
const tempLink = 'https://students2.uowm.gr';
const loginUrl = environment.usersUrl;
const logoutUrl = `${environment.logOutUrl}`;
class ExternalLogin extends Component {
  constructor(props) {
    super(props);
  }

  state = {
    url: link,
    gotData: false,
  };

  componentDidMount() {
    const service = this.props.route.params.service;
    if (service === 'login') {
      this.setState({url: tempLink});
    } else {
      this.setState({url: logoutUrl});
    }
  }

  onLoad = async (webViewState: {url: string}) => {
    if (
      webViewState.url.startsWith('https://students2.uowm.gr/#/auth/callback')
    ) {
      if (!this.state.gotData) {
        try {
          this.setState({gotData: true});
          const temp = webViewState.url.split('&')[2];
          const uToken = temp.split('=')[1];
          const info = await fetchUniversisInfo(uToken);
          const userByEmail = await fetchUserByEmail(info.email);
          if (userByEmail) {
            const fcmToken = await getFCMToken();
            const user = {
              identifier: userByEmail.identifier,
              fcmToken,
            };

            const token = await userExternalLogin(user);
            await storeToken(token);
            const userInfo = await fetchUserInfo();
            this.props.setUser(userInfo);
            this.props.changeLoggedIn(true);
            this.props.navigation.navigate('stores');
          } else {
            this.props.navigation.navigate('register', {info});
          }
        } catch (e) {
          console.log('i got error');
          console.log(e);
        }
      }
    }
  };

  onShouldStartLoadWithRequest = (request: {url: string}) => {
    if (Platform.OS === 'android') {
      return !request.url.startsWith('https://students.universis.io');
    }
    return true;
  };

  render() {
    return (
      <WebView
        originWhitelist={['*']}
        style={styles.webview}
        sharedCookiesEnabled={true}
        javaScriptEnabled={true}
        thirdPartyCookiesEnabled={true}
        onNavigationStateChange={webViewState => this.onLoad(webViewState)}
        source={{uri: this.state.url}}
        mixedContentMode={'compatibility'}
      />
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  webview: {
    alignSelf: 'stretch',
    flex: 1,
  },
  video: {
    marginTop: 20,
    maxHeight: 200,
    width: 320,
    flex: 1,
  },
});

const mapStateToProps = state => {
  return {actions: state.actions};
};

const mapDispatchToProps = dispatch => {
  return {
    changeLoading: value => dispatch(changeLoading(value)),
    errorHandling: value => dispatch(errorHandling(value)),
    changeLoggedIn: value => dispatch(changeLoggedIn(value)),
    setUser: value => dispatch(setUser(value)),
    showMessage: value => dispatch(showMessage(value)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ExternalLogin);
