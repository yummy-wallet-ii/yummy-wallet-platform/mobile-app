import React, {useEffect, useState} from 'react';
import {Animated, ImageBackground, Text, View} from 'react-native';
import {styles, appColors, appWidth, appHeight, fonts} from '../../styles';
import Swiper from 'react-native-swiper';
import {Avatar, Button} from 'react-native-paper';
import {useNavigation} from '@react-navigation/native';
import Easing from 'react-native/Libraries/Animated/Easing';
let boardListItems = [
  {
    id: 1,
    image: 'home',
    hasBackground: false,
    caption: 'Καλώς ήρθες στο Yummy Wallet',
    text: 'Την εφαρμογή που σου επιστρέφει πραγματικά μετρητά με κάθε αγορά σου',
    easing: Easing.bounce,
    opacity: new Animated.Value(0),
  },
  {
    id: 2,
    image: 'account',
    hasBackground: true,
    caption: 'Κάνε δωρεάν εγγραφή',
    text: 'Δημιούργησε δωρέαν λογαριασμό και απόκτησε τον Yummy κωδικό σου',
    easing: Easing.bounce,
    opacity: new Animated.Value(0),
  },
  {
    id: 3,
    image: 'cart',
    hasBackground: false,
    caption: 'Ψώνισε από τα Yummy Spots',
    text: 'Κάνε τις αγορές σου στα καταστήματα του Yummy Wallet',
    easing: Easing.bounce,
    opacity: new Animated.Value(0),
  },
  {
    id: 4,
    image: 'message-processing',
    hasBackground: false,
    caption: 'Πες τον κωδικό σου',
    text: 'Πλήρωσε με όποιον τρόπο θέλεις και στο ταμείο πες τον Yummy κωδικό σου',
    easing: Easing.bounce,
    opacity: new Animated.Value(0),
  },
  {
    id: 5,
    image: 'credit-card-outline',
    hasBackground: false,
    caption: 'Γέμισε το ψηφιακό σου πορτοφόλι',
    text: 'Με κάθε αγορά προστίθεται άμεσα στο λογαριασμό σου το ποσό της επιστροφής',
    easing: Easing.bounce,
    opacity: new Animated.Value(0),
  },
  {
    id: 6,
    image: 'help',
    hasBackground: true,
    caption: 'Θέλεις να μάθεις περισσότερα;',
    text: 'Μάθε περισσότερα για το Yummy Wallet διαβάζοντας τις Συχνές Ερωτήσεις',
    easing: Easing.bounce,
    opacity: new Animated.Value(0),
  },
];
export const AppBoard = () => {
  const [previousIndex, setPreviousIndex] = useState(-1);
  const [isLoaded, setIsLoaded] = useState(false);
  useEffect(() => {
    if (!isLoaded) {
      animate(0);
    }
  });

  const animate = index => {
    let item = boardListItems[index];
    item.opacity.setValue(0);
    Animated.timing(item.opacity, {
      useNativeDriver: false,
      toValue: 1,
      duration: 1200,
      easing: Easing.sin,
    }).start(() => {
      if (previousIndex === -1) {
        setPreviousIndex(index);
        setIsLoaded(true);
      }

      if (index !== previousIndex && previousIndex !== -1) {
        boardListItems[previousIndex].opacity.setValue(0);
        setPreviousIndex(index);
      }
    });
  };

  const boardList = boardListItems.map(item => {
    return (
      <View
        key={item.id}
        style={[styles.mainContainer, {backgroundColor: appColors.mainBlue}]}>
        <View
          style={{
            flexDirection: 'row',
            flex: 0.8,
            backgroundColor: 'transparent',
          }}>
          <ImageBackground
            style={{
              justifyContent: 'center',
              alignItems: 'center',
              flex: 1,
              flexDirection: 'column',
            }}
            imageStyle={{width: appWidth, height: '100%'}}
            source={require('../../assets/images/whitebg.png')}
            resizeMode={'cover'}>
            <Animated.View style={{opacity: item.opacity}}>
              <Avatar.Icon
                size={item.hasBackground ? 128 : 256}
                color={item.hasBackground ? 'white' : appColors.mainBlue}
                style={{
                  backgroundColor: item.hasBackground
                    ? appColors.mainBlue
                    : 'transparent',
                }}
                icon={item.image}
              />
            </Animated.View>
          </ImageBackground>
        </View>

        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <View
            style={{
              flexDirection: 'column',
              justifyContent: 'center',
              alignItems: 'center',
              width: '75%',
            }}>
            <Text
              style={{
                textAlign: 'center',
                color: 'white',
                fontWeight: '600',
                marginBottom: 10,
                fontSize: 18,
                fontFamily: fonts.italic
              }}>
              {item.caption}
            </Text>
            <Text style={{color: 'white', textAlign: 'center', fontSize: 16, fontFamily: fonts.light}}>
              {item.text}
            </Text>

            {item.id === 6 ? (
              <View
                style={{
                  position: 'absolute',
                  top: appHeight * 0.15,
                  flexDirection: 'row',
                  justifyContent: 'space-evenly',
                }}>
                {/*<Button*/}
                {/*  mode="text"*/}
                {/*  color={'white'}*/}
                {/*  uppercase={false}*/}
                {/*  onPress={() => navigateTo('help')}>*/}
                {/*  Ναι θέλω!*/}
                {/*</Button>*/}

                <Button
                  labelStyle={{color: appColors.iceWhite, fontFamily: fonts.light}}
                  uppercase={false}
                  mode="text"
                  onPress={() => navigateTo('dashboard')}>
                  Είμαι έτοιμος!
                </Button>
              </View>
            ) : null}
          </View>
        </View>
      </View>
    );
  });
  const navigation = useNavigation();
  const navigateTo = route => {
    navigation.navigate(route);
  };
  return (
    <Swiper
      loop={false}
      dotStyle={{backgroundColor: 'white', opacity: 0.3}}
      activeDotStyle={{backgroundColor: 'white'}}
      onIndexChanged={index => {
        animate(index);
      }}
      showsButtons={false}>
      {boardList}
    </Swiper>
  );
};
