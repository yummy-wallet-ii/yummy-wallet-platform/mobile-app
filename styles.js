import {Dimensions, StyleSheet} from 'react-native';
export const appWidth = Dimensions.get('window').width;
export const appHeight = Dimensions.get('window').height;
export const appColors = {
  // mainBlue: '#2196F3',
  blue: 'red',
  black: '#171717',
  mainBlue: '#1ba9ae',
  mainRed: 'red',
  mainGreen: 'green',
  secondaryBlue: 'lightblue',
  mainGray: 'gray',
  secondaryGray: '#55A4BD',
  mainWhite: 'white',
  transparent: 'transparent',
  // pencil: '#6d6e71',
  pencil: '#ffffff',
  iceWhite: '#F8F8FF',
  bordeau: '#d11e51',
  orange: '#faa626',
  // orange: '#ffffff',
};

export const fonts = {
  regular: 'Roboto-Regular',
  bold: 'Roboto-Bold',
  light: 'Roboto-Light',
  italic: 'Roboto-Italic',
  space: 'SpaceMono-Regular'
}

export const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: appColors.transparent,
  },
  image: {
    flex: 1,
    justifyContent: 'center',
  },
  container: {
    flex: 1,
  },
  rowView: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  separator: {
    height: 0.3,
    width: '100%',
    backgroundColor: appColors.mainGray,
    opacity: 0.8,
  },
  boldText: {
    fontWeight: 'bold',
  },
  contentContainerStyle: {
    paddingBottom: 200,
  },
  contentContainerStyle2: {
    paddingBottom: 100,
  },
});
