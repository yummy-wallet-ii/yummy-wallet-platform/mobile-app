// import 'react-native-gesture-handler';
import React, {useEffect} from 'react';
import {StatusBar, YellowBox} from 'react-native';
import {SafeAreaProvider} from 'react-native-safe-area-context/src/SafeAreaContext';
import Loading from './helpers/Loading';
import GlobalErrorHandler from './helpers/GlobalErrorHandler';
import {Provider as PaperProvider} from 'react-native-paper';
import {Provider as StoreProvider} from 'react-redux';
import configureStore from './storage/store';
const store = configureStore();
import {LogBox} from 'react-native';
LogBox.ignoreLogs([
  "[react-native-gesture-handler] Seems like you're using an old API with gesture components, check out new Gestures system!",
]);
LogBox.ignoreAllLogs(true);

import {RootNavigation} from './navigation/RootNavigation';
import SplashScreen from 'react-native-splash-screen';
import {NotificationListener} from './services/pushNotificationService';
import DisplaySnackBar from './helpers/DisplaySnackBar';
import messaging from '@react-native-firebase/messaging';
import {showMessage} from './storage/actions/actions';
export const App = () => {

  useEffect(() => {
    SplashScreen.hide();
    // checkPermission()
    NotificationListener();
    const unsubscribe = messaging().onMessage(async remoteMessage => {
      let message;
      if (remoteMessage.notification) {
        message = remoteMessage.notification;
      } else {
        message = {
          body: remoteMessage.data.alert,
          title: remoteMessage.title,
        };
      }
      store.dispatch(showMessage({visible: true, message: message}));
    });

    return unsubscribe;
  }, []);

  return (
    <SafeAreaProvider>
      <StoreProvider store={store}>
        <PaperProvider>
          <Loading />
          <GlobalErrorHandler />
          <DisplaySnackBar />
          <StatusBar hidden={true} />
          <RootNavigation />
        </PaperProvider>
      </StoreProvider>
    </SafeAreaProvider>
  );
};
