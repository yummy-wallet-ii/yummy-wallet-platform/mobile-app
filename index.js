/**
 * @format
 */

import {AppRegistry} from 'react-native';
// import messaging from '@react-native-firebase/messaging';

import {name as appName} from './app.json';

// messaging().setBackgroundMessageHandler(async remoteMessage => {
//     console.log('Message handled in the background!', remoteMessage);
// });
// import {initialize} from './services/oneSignalService';
import {App} from "./App";

// initialize();
    AppRegistry.registerComponent(appName, () => App);


