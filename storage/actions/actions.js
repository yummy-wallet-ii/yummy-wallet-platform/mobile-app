import {
  DRAWER_ITEM,
  ERROR_HANDLER,
  SHOW_MESSAGE,
  INIT_LOGOUT,
  IS_LOGGED_IN,
  LOADING_CHANGE,
  SET_NOTIFICATION,
  SET_STORES,
  SET_USER,
  SET_ORDER,
  SET_BOTTOM_BAR_STATUS, SET_CART,
} from '../constants';

export const changeLoading = loading => ({
  type: LOADING_CHANGE,
  payload: loading,
});

export const errorHandling = data => ({
  type: ERROR_HANDLER,
  payload: data,
});

export const showMessage = data => ({
  type: SHOW_MESSAGE,
  payload: data,
});

export const setNotification = data => ({
  type: SET_NOTIFICATION,
  payload: data,
});

export const setDrawerItem = data => ({
  type: DRAWER_ITEM,
  payload: data,
});

export const changeLoggedIn = data => ({
  type: IS_LOGGED_IN,
  payload: data,
});

export const isBottomVisible = data => ({
  type: SET_BOTTOM_BAR_STATUS,
  payload: data,
});

export const initLoggingOut = data => ({
  type: INIT_LOGOUT,
  payload: data,
});
export const setStores = data => ({
  type: SET_STORES,
  payload: data,
});

export const setCart = data => ({
  type: SET_CART,
  payload: data,
});

export const setUser = data => ({
  type: SET_USER,
  payload: data,
});

export const setOrder = data => ({
  type: SET_ORDER,
  payload: data,
});
