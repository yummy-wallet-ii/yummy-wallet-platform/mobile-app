import {
  DRAWER_ITEM,
  ERROR_HANDLER,
  INIT_LOGOUT,
  IS_LOGGED_IN,
  LOADING_CHANGE,
  ROLE,
  SET_BOTTOM_BAR_STATUS,
  SET_NOTIFICATION,
  SET_ORDER,
  SET_STORES,
  SET_USER,
  SHOW_MESSAGE,
  SET_CART,
} from '../constants';

const initialState = {
  loading: false,
  isLoggedIn: false,
  loggingOut: false,
  errorHandler: {visible: false, message: ''},
  displayMessage: {visible: false, message: ''},
  hasNotification: false,
  drawerItem: '',
  stores: [],
  user: null,
  orders: [],
  isBottomBarVisible: true,
  cart: {},
};

const actionsReducer = (state = initialState, action) => {
  switch (action.type) {
    case LOADING_CHANGE: {
      return {...state, loading: action.payload};
    }
    case ERROR_HANDLER: {
      return {...state, errorHandler: action.payload};
    }
    case SHOW_MESSAGE: {
      return {...state, displayMessage: action.payload};
    }
    case SET_NOTIFICATION: {
      return {...state, hasNotification: action.payload};
    }
    case DRAWER_ITEM: {
      return {...state, drawerItem: action.payload};
    }
    case ROLE: {
      return {...state, role: action.payload};
    }
    case IS_LOGGED_IN: {
      return {...state, isLoggedIn: action.payload};
    }
    case INIT_LOGOUT: {
      return {...state, loggingOut: action.payload};
    }
    case SET_STORES: {
      return {...state, stores: action.payload};
    }
    case SET_USER: {
      return {...state, user: action.payload};
    }
    case SET_ORDER: {
      return {...state, orders: action.payload};
    }
    case SET_CART: {
      return {...state, cart: action.payload};
    }
    case SET_BOTTOM_BAR_STATUS: {
      return {...state, isBottomBarVisible: action.payload};
    }
    default:
      return state;
  }
};

export default actionsReducer;
