import React from 'react';
import {useDispatch, useSelector} from 'react-redux';
import {showMessage} from '../storage/actions/actions';
import {Portal, Snackbar} from 'react-native-paper';
import {appColors} from '../styles';
import {Text, View} from 'react-native';

const DisplaySnackBar = () => {
  const state = useSelector(state => state);
  const dispatch = useDispatch();

  /**
   * Closes the dialog.
   */
  const handle = () => {
    dispatch(showMessage({visible: false, message: ''}));
  };

  return (
    <Portal>
      <Snackbar
        // wrapperStyle={{top: 0}}
        duration={2000}
        style={{backgroundColor: appColors.mainWhite}}
        visible={state.actions.displayMessage.visible}
        icon={'home'}
        onDismiss={handle}>
        <View style={{flexDirection: 'row'}}>
          <Text style={{color: appColors.mainBlue}}>
            {state.actions.displayMessage.message.body}
          </Text>
        </View>
      </Snackbar>
    </Portal>
  );
};

export default DisplaySnackBar;
