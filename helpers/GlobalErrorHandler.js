import React from 'react';
import {useDispatch, useSelector} from 'react-redux';
import {errorHandling} from '../storage/actions/actions';
import {Dialog, Paragraph, Portal} from 'react-native-paper';
import {Button} from 'react-native';

const GlobalErrorHandler = () => {
  const state = useSelector(state => state);
  const dispatch = useDispatch();

  /**
   * Closes the dialog.
   */
  const handle = () => {
    dispatch(errorHandling({visible: false, message: ''}));
  };

  return (
    <Portal>
      <Dialog visible={state.actions.errorHandler.visible} onDismiss={handle}>
        <Dialog.Content>
          <Paragraph> {state.actions.errorHandler.message}</Paragraph>
        </Dialog.Content>
        <Dialog.Actions>
          <Button onPress={handle} title={'OK'} color={'#1D8391'} />
        </Dialog.Actions>
      </Dialog>
    </Portal>
  );
};

export default GlobalErrorHandler;
