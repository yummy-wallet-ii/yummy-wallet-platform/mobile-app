import messaging from '@react-native-firebase/messaging';
import AsyncStorage from '@react-native-async-storage/async-storage';

async function requestUserPermission() {
  const authStatus = await messaging().requestPermission();
  const enabled =
    authStatus === messaging.AuthorizationStatus.AUTHORIZED ||
    authStatus === messaging.AuthorizationStatus.PROVISIONAL;

  if (enabled) {
    console.log('Authorization status:', authStatus);
  }
}

async function clearFCMToken() {
  try {
    await AsyncStorage.removeItem('fcmToken');
  } catch (e) {
    console.log('error while clearing fcm token');
  }
}

async function getFCMToken() {
  let fcmToken = await AsyncStorage.getItem('fcmToken');

  try {
    if (!fcmToken) {
      fcmToken = await messaging().getToken();
      if (fcmToken) {
        await AsyncStorage.setItem('fcmToken', fcmToken);
      }
    }

    return fcmToken;
  } catch (e) {
    console.log('error in fcm token');
  }
}

const NotificationListener = () => {
  messaging().onNotificationOpenedApp(remoteMessage => {
    console.log('Notification caused app to open from background state:');
    console.log(remoteMessage.notification);
  });

  messaging()
    .getInitialNotification()
    .then(remoteMessage => {
      if (remoteMessage) {
        console.log(
          'Notification caused app to open from quit state:',
          remoteMessage.notification,
        );
        // setInitialRoute(remoteMessage.data.type); // e.g. "Settings"
      }
      // setLoading(false);
    });

  // messaging().onMessage(async remoteMessage => {
  //   console.log('notification on foreground');
  //   // console.log(remoteMessage);
  // });
};

const checkPermission = async () => {
  const enabled = await messaging().hasPermission();
  if (enabled) {
    await this.getFCMToken();
  } else {
    this.requestPermission();
  }
};

const requestPermission = async () => {
  try {
    await messaging().requestPermission();
    // User has authorised
  } catch (error) {
    // User has rejected permissions
  }
};

module.exports = {
  getFCMToken,
  clearFCMToken,
  checkPermission,
  NotificationListener,
  requestUserPermission,
  requestPermission
};
