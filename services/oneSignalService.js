import {OneSignal} from 'react-native-onesignal';
import Geolocation from '@react-native-community/geolocation';

const sentNotifications = [];
const APP_ID = 'b83c8010-dbde-4121-a345-ef946b84f78a';
const DISTANCE_THRESHOLD = 100;
function initialize() {

// OneSignal Initialization
    OneSignal.initialize(APP_ID);

// requestPermission will show the native iOS or Android notification permission prompt.
// We recommend removing the following code and instead using an In-App Message to prompt for notification permission
    OneSignal.Notifications.requestPermission(true);

// Method for listening for notification clicks
    OneSignal.Notifications.addEventListener('click', (event) => {
        console.log('OneSignal: notification clicked:', event);
    });
  // OneSignal.setAppId(APP_ID);
  // OneSignal.setLocationShared(true);
  // OneSignal.promptLocation();
  //
  // console.log('One Signal Initialized');
  //
  // // promptForPushNotificationsWithUserResponse will show the native iOS or Android notification permission prompt.
  // // We recommend removing the following code and instead using an In-App Message to prompt for notification permission (See step 8)
  // OneSignal.promptForPushNotificationsWithUserResponse();
  //
  // //Method for handling notifications received while app in foreground
  // OneSignal.setNotificationWillShowInForegroundHandler(
  //   notificationReceivedEvent => {
  //     console.log(
  //       'OneSignal: notification will show in foreground:',
  //       notificationReceivedEvent,
  //     );
  //     let notification = notificationReceivedEvent.getNotification();
  //     console.log('notification: ', notification);
  //     const data = notification.additionalData;
  //     console.log('additionalData: ', data);
  //     // Complete with null means don't show a notification.
  //     notificationReceivedEvent.complete(notification);
  //   },
  // );
  //
  // //Method for handling notifications opened
  // OneSignal.setNotificationOpenedHandler(notification => {
  //   console.log('OneSignal: notification opened:', notification);
  // });
}

function fetchFences() {
  OneSignal.setLogLevel(6, 0);
}

async function getOneSignalPlayerId() {
  const deviceState = await OneSignal.getDeviceState();
  return deviceState.userId;
}

const sendPushNotification = (playerId, storeId, targetLat, targetLng) => {
  Geolocation.getCurrentPosition(
    position => {
      const {latitude, longitude} = position.coords;
      const targetLatitude = targetLat;
      const targetLongitude = targetLng;
      const distance = calculateDistance(
        latitude,
        longitude,
        targetLatitude,
        targetLongitude,
      );
      if (distance <= DISTANCE_THRESHOLD) {
        if (sentNotifications.find(n => n.storeId)) {
          return;
        }
        try {
          sentNotifications.push({storeId});

          const data = {
            app_id: APP_ID,
            contents: {en: 'Νέες προσφορές'},
            headings: {en: 'Προλάβετε Τώρα!'},
            include_player_ids: [playerId],
          };

          OneSignal.postNotification(
            JSON.stringify(data),
            success => {
              console.log('Notification sent successfully:', success);
            },
            error => {
              console.log('Error sending notification:', error);
            },
          );
          // OneSignal.postNotification({
          //   contents: {en: 'You are near the target location'},
          //   include_player_ids: [playerId],
          //   data: JSON.stringify({route: 'target'}),
          // });
          console.log('message sent');
        } catch (e) {
          console.log(e);
        }
      }
    },
    error => console.log(error),
    {enableHighAccuracy: true, timeout: 20000, maximumAge: 1000},
  );
};

/**
 *
 * @param lat1
 * @param lon1
 * @param lat2
 * @param lon2
 * @returns {number}
 */
const calculateDistance = (lat1, lon1, lat2, lon2) => {
  const R = 6371; // radius of the earth in km
  const dLat = deg2rad(lat2 - lat1);
  const dLon = deg2rad(lon2 - lon1);
  const a =
    Math.sin(dLat / 2) * Math.sin(dLat / 2) +
    Math.cos(deg2rad(lat1)) *
      Math.cos(deg2rad(lat2)) *
      Math.sin(dLon / 2) *
      Math.sin(dLon / 2);
  const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
  const d = R * c; // distance in km
  return d * 1000; // distance in meters
};

const deg2rad = deg => {
  return deg * (Math.PI / 180);
};

// module.exports = {
//   initialize,
//   fetchFences,
//   getOneSignalPlayerId,
//   sendPushNotification,
// };
