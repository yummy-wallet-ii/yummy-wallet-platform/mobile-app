import AsyncStorage from '@react-native-async-storage/async-storage';
import {config} from '../environments/environment';
const apiUrl = config.apiUrl;
// const apiUrl = 'http://192.168.1.5:9000/api/v1/';
// const apiUrl = 'https://yummy-wallet-server.ysoft.gr/api/v1/';

/**
 *
 * @param endpoint
 * @returns {Promise<any>}
 */
async function getRequest(endpoint = '') {
  const token = await retrieveToken();
  const response = await fetch(`${apiUrl}${endpoint}`, {
    method: 'GET',
    headers: {
      Authorization: `Bearer ${token}`,
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
  });
  return await response.json();
}

/**
 *
 * @param list
 */
function listSort(list) {
  list.sort(function (a, b) {
    return ('' + a.description).localeCompare(b.description);
  });
}

/**
 *
 * @param token
 * @returns {Promise<void>}
 */
async function storeToken(token) {
  await AsyncStorage.setItem('token', token);
}

/**
 *
 * @returns {Promise<null|*>}
 */
async function retrieveToken() {
  try {
    const token = AsyncStorage.getItem('token');
    return token ? token : null;
  } catch (e) {
    return null;
  }
}

/**
 *
 * @returns {Promise<void>}
 */
async function removeToken() {
  AsyncStorage.removeItem('token');
}

/**
 *
 * @param endpoint
 * @param data
 * @returns {Promise<any>}
 */
async function postRequest(endpoint = '', data = {}) {
  const token = await retrieveToken();
  const response = await fetch(`${apiUrl}${endpoint}`, {
    method: 'POST',
    headers: {
      Authorization: `Bearer ${token}`,
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(data),
  });
  if (response.status === 200) {
    return await response.json();
  }
  if (response.status >= 400) {
    throw await response.json();
  }
}

/**
 *
 * @param endpoint
 * @param data
 * @returns {Promise<any>}
 */
async function putRequest(endpoint = '', data = {}) {
  const token = await retrieveToken();
  const response = await fetch(`${apiUrl}${endpoint}`, {
    method: 'PUT',
    headers: {
      Authorization: `Bearer ${token}`,
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(data),
  });
  return await response.json();
}

/**
 *
 * @param endpoint
 * @returns {Promise<any>}
 */
async function deleteRequest(endpoint = '') {
  const token = await retrieveToken();

  const response = await fetch(`${apiUrl}${endpoint}`, {
    method: 'DELETE',
    headers: {
      Authorization: `Bearer ${token}`,
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
  });
  return await response.json();
}

module.exports = {
  getRequest,
  postRequest,
  putRequest,
  deleteRequest,
  storeToken,
  retrieveToken,
  removeToken,
  listSort,
};
