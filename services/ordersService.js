import {getRequest, postRequest} from './commonService';

/**
 *
 * @param order
 * @returns {Promise<*>}
 */
async function createOrder(order) {
  return await postRequest('orders', order);
}

/**
 *
 * @returns {Promise<*>}
 */
async function fetchPendingOrders() {
  return await getRequest('orders/pending/customer');
}

/**
 *
 * @returns {Promise<*>}
 */
async function fetchCompletedOrders() {
  return await getRequest('orders/completed/customer');
}

module.exports = {
  createOrder,
  fetchPendingOrders,
  fetchCompletedOrders,
};
