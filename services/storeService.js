const {getRequest, postRequest, deleteRequest} = require('./commonService');
const fetchStores = async () => {
  return await getRequest(`stores`);
};

const fetchStoreById = async (storeId) => {
  return await getRequest(`stores/customer/${storeId}`);
};


const fetchImage = async storeId => {
  return await getRequest(`stores/frontImage/data/${storeId}`);
};

const isFavoriteStore = async storeId => {
  const isFavoriteRequest = await getRequest(`customers/favourite/${storeId}`);
  return isFavoriteRequest.favorite;
};

const addFavoriteStore = async storeId => {
  return await postRequest(`customers/favourite/`, {storeId});
};

const removeFavoriteStore = async storeId => {
  return await deleteRequest(`customers/favourite/${storeId}`);
};

const fetchFavoriteStores = async () => {
  return await getRequest(`customers/favourite`);
};

module.exports = {
  fetchStores,
  fetchStoreById,
  isFavoriteStore,
  fetchImage,
  addFavoriteStore,
  removeFavoriteStore,
  fetchFavoriteStores,
};
