import {postRequest} from './commonService';

const createTransaction = async params => {
  return await postRequest('transactions', params);
};

module.exports = {
  createTransaction,
};
