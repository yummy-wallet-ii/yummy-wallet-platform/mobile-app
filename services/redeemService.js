import {getRequest, postRequest} from "./commonService";

export async function fetchRedeemRequests() {
    return await getRequest('redeem/me');
}

async function createRedeemRequest(body) {
    return await postRequest('redeem', body);

}

module.exports = {fetchRedeemRequests, createRedeemRequest}
