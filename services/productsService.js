import {getRequest} from './commonService';

async function fetchProductsByCategoryId(storeId, categoryId) {
  return await getRequest(
    `products/customer?storeId=${storeId}&categoryId=${categoryId}`,
  );
}

async function fetchStampsByCategoryId(storeId, categoryId) {
  return await getRequest(
    `products/customer/stamps?storeId=${storeId}&categoryId=${categoryId}`,
  );
}

async function fetchProductImage(productId) {
  return await getRequest(`products/${productId}/image`);
}

module.exports = {
  fetchProductsByCategoryId,
  fetchProductImage,
  fetchStampsByCategoryId
};
