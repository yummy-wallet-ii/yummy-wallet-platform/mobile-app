import {removeToken, postRequest, getRequest} from './commonService';
import {config} from '../environments/environment';

/**
 *
 * @param user
 * @returns {Promise<*>}
 */
async function userLogin(user) {
  return await postRequest('customers/login', user);
}

async function userExternalLogin(user) {
  return await postRequest('customers/login/external', user);
}

const apiUrl = config.apiUrl;

async function fetchUniversisInfo(token) {
  const response = await fetch(`${apiUrl}customers/universis`, {
    method: 'GET',
    headers: {
      Authorization: `Bearer ${token}`,
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
  });
  return await response.json();
}

async function fetchUserByEmail(email) {
  return await getRequest(`customers/email?email=${email}`);
}

/**
 *
 * @param user
 * @returns {Promise<*>}
 */
async function userRegistration(user) {
  return await postRequest('customers', user);
}

/**
 *
 * @returns {Promise<*>}
 */
async function fetchUserInfo() {
  return await getRequest('customers/me');
}

/**
 *
 * @returns {Promise<*>}
 */
async function fetchUserTransactions() {
  return await getRequest('transactions/customer');
}

/**
 *
 * @returns {Promise<*>}
 */
async function fetchUserCredits() {
  return await getRequest('credits/me');
}

/**
 *
 * @returns {Promise<void>}
 */
async function userLogout() {
  await removeToken();
}

module.exports = {
  userLogin,
  userLogout,
  userRegistration,
  fetchUserInfo,
  fetchUserTransactions,
  fetchUserCredits,
  fetchUniversisInfo,
  fetchUserByEmail,
  userExternalLogin,
};
