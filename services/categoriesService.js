import {getRequest} from './commonService';

/**
 *
 * @param storeId
 * @returns {Promise<any>}
 */
async function fetchStoreCategories(storeId) {
  return await getRequest(`categories/customer?storeId=${storeId}`);
}

/**
 *
 * @param categoryId
 * @returns {Promise<any>}
 */
async function fetchCategoryImage(categoryId) {
  return await getRequest(`categories/fetchCategoryImage/${categoryId}`);
}

module.exports = {
  fetchStoreCategories,
  fetchCategoryImage,
};
