import React, {Component} from 'react';
import {Appbar, Text, TextInput} from 'react-native-paper';
import {appColors, appWidth} from '../styles';

export class SearchBar extends Component {
  constructor(props) {
    super(props);
  }

  searchHandling = data => {
    this.props.parentCallback(data);
  };

  render() {
    return (
      <Appbar
        style={{
          backgroundColor: 'transparent',
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <TextInput
          style={{
            width: appWidth * 0.9,
            height: 40,
            backgroundColor: 'transparent',
            color: appColors.mainWhite,
          }}
          selectionColor={appColors.mainWhite}
          activeOutlineColor={appColors.mainWhite}
          activeUnderlineColor={appColors.mainWhite}
          placeholderTextColor={appColors.mainWhite}
          placeholder="Αναζήτηση καταστημάτων"
          left={<TextInput.Icon name={'magnify'} color={appColors.mainWhite} />}
          onChangeText={text => {
            this.searchHandling(text);
          }}
        />
      </Appbar>
    );
  }
}
