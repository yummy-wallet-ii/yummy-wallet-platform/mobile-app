import React from 'react';
import {Appbar} from 'react-native-paper';
import {useNavigation} from '@react-navigation/native';
import {fonts} from "../styles";

export const SimpleBackHeader = ({
  title,
  isTransactions = true,
  bgColor = 'transparent',
}) => {
  const navigation = useNavigation();

  const goBack = () => {
    navigation.goBack();
  };
  const navigateHome = () => {
    navigation.navigate('stores');
  };
  return (
    <Appbar.Header style={{backgroundColor: bgColor}}>
      {isTransactions ? (
        <Appbar.Action
          icon={'home'}
          size={22}
          onPress={navigateHome}
          color={'white'}
        />
      ) : (
        <Appbar.BackAction onPress={goBack} color={'white'} />
      )}

      <Appbar.Content title={title} subtitle="" color={'white'} titleStyle={{fontFamily: fonts.light}}/>
    </Appbar.Header>
  );
};
