import React from 'react';
import {Appbar} from 'react-native-paper';
import {useNavigation} from '@react-navigation/native';
import {appColors} from '../styles';
import {useSelector} from 'react-redux';

export const ShoppingCartBackHeader = ({
  title,
  displayConfirmDelete,
  storeId,
}) => {
  const navigation = useNavigation();
  const state = useSelector(state => state);

  const goBack = () => {
    navigation.goBack();
  };

  return (
    <Appbar.Header
      style={{backgroundColor: appColors.transparent, elevation: 0}}>
      <Appbar.BackAction onPress={goBack} color={'white'} />
      <Appbar.Content title={title} subtitle="" color={'white'} />
      {state.actions.isLoggedIn && state.actions.cart[storeId] ? (
        <Appbar.Action
          icon={'delete'}
          disabled={state.actions.cart[storeId].orderItems.length === 0}
          size={26}
          onPress={() => {
            displayConfirmDelete(true);
          }}
          color={'red'}
        />
      ) : null}
    </Appbar.Header>
  );
};
