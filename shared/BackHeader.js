import React from 'react';
import {Appbar, Badge} from 'react-native-paper';
import {useNavigation} from '@react-navigation/native';
import {useSelector} from 'react-redux';
import {View} from 'react-native';
import {fonts} from "../styles";

export const BackHeader = ({
  title,
  storeId,
  publicKey,
  hasStatmps = false,
  showStamps,
  hasCoupons = false,
  showCoupons,
}) => {
  const navigation = useNavigation();
  const actions = useSelector(state => state.actions);

  const goBack = () => {
    navigation.goBack();
  };

  const goToCart = () => {
    navigation.navigate('cart', {storeId, publicKey});
  };

  return (
    <Appbar.Header style={{backgroundColor: 'transparent'}}>
      <Appbar.BackAction onPress={goBack} color={'white'} />
      <Appbar.Content title={title} subtitle="" color={'white'} titleStyle={{fontFamily: fonts.light}}/>
      {actions.isLoggedIn && actions.cart[storeId] ? (
        <View style={{flexDirection: 'row'}}>
          {hasCoupons ? (
            <Appbar.Action
              icon={'ticket-percent'}
              size={26}
              onPress={showCoupons}
              color={'white'}
            />
          ) : null}
          {hasStatmps ? (
            <Appbar.Action
              icon={'postage-stamp'}
              size={26}
              onPress={showStamps}
              color={'white'}
            />
          ) : null}

          {actions.cart[storeId]?.orderItems.length > 0 ? (
            <Badge
              visible={true}
              size={20}
              style={{position: 'absolute', top: 5, right: 5, zIndex: 9}}>
              {actions.cart[storeId]?.orderItems.length}
            </Badge>
          ) : null}

          <Appbar.Action
            icon={'cart'}
            size={26}
            onPress={goToCart}
            color={
              actions.cart[storeId].orderItems?.length > 0
                ? 'lightgreen'
                : 'white'
            }
          />
        </View>
      ) : null}
    </Appbar.Header>
  );
};
