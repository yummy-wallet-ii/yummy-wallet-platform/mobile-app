import React, {useEffect, useState} from 'react';
import {View} from 'react-native';
import * as Animatable from 'react-native-animatable';
import {appColors, styles} from '../styles';

export default function ColorScreen({route, navigation}) {
  const viewRef = React.useRef(null);
  const [bgColor, setBgColor] = useState();
  useEffect(() => {
    switch (route.name) {
      case 'Home': {
        setBgColor(appColors.mainBlue);
        break;
      }
      case 'Search': {
        setBgColor(appColors.mainGreen);
        break;
      }
      case 'Add': {
        setBgColor(appColors.mainGray);
        break;
      }
      case 'Account': {
        setBgColor(appColors.mainRed);
        break;
      }
      case 'Like': {
        setBgColor(appColors.mainBlue);
        break;
      }
      default:
        setBgColor(appColors.mainWhite);
    }
  }, []);
  useEffect(() => {
    const unsubscribe = navigation.addListener('focus', () => {
      viewRef.current.animate({0: {opacity: 0.5}, 1: {opacity: 1}});
    });
    return () => unsubscribe;
  }, [navigation]);
  return (
    <View style={styles.container}>
      {/*<MyHeader*/}
      {/*  menu*/}
      {/*  onPressMenu={() => navigation.goBack()}*/}
      {/*  title={route.name}*/}
      {/*  right="more-vertical"*/}
      {/*  onRightPress={() => console.log('right')}*/}
      {/*/>*/}
      <Animatable.View
        ref={viewRef}
        easing={'ease-in-out'}
        style={styles.container}>
        <View style={{backgroundColor: bgColor, flex: 1}} />
      </Animatable.View>
    </View>
  );
}
