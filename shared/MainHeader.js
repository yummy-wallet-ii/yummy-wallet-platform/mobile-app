import React from 'react';
import {Appbar} from 'react-native-paper';
import {appColors, fonts} from '../styles';

export const MainHeader = ({title}) => {
  return (
    <Appbar.Header
      style={{backgroundColor: appColors.transparent, elevation: 0}}>
      <Appbar.Content title={title} subtitle="" color={'white'} titleStyle={{fontFamily: fonts.light}}/>
    </Appbar.Header>
  );
};
