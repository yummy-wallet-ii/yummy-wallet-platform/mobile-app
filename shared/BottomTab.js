import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import React, {useEffect, useRef, useState} from 'react';
import {StyleSheet, TouchableOpacity, View} from 'react-native';
import * as Animatable from 'react-native-animatable';
import {appColors, fonts} from '../styles';
import Stores from '../appScreens/mainScreens/Stores';
import SearchStores from '../appScreens/mainScreens/SearchStores';
import Favorites from '../appScreens/mainScreens/Favorites';
import Account from '../appScreens/authScreens/Account';
import ScanPage from '../appScreens/mainScreens/ScanPage';
import {useSelector} from 'react-redux';
import Icon from "react-native-vector-icons/MaterialCommunityIcons";

const TabArr = [
  {
    route: 'stores',
    label: 'Καταστήματα',
    type: 'MaterialCommunityIcons',
    icon: 'store-outline',
    component: Stores,
  },
  {
    route: 'search',
    label: 'Αναζήτηση',
    type: 'MaterialCommunityIcons',
    icon: 'magnify',
    component: SearchStores,
  },
  {
    route: 'scan',
    label: 'QR',
    type: 'MaterialCommunityIcons',
    icon: 'qrcode-scan',
    component: ScanPage,
  },
  {
    route: 'account',
    label: 'Λογαριασμός',
    type: 'MaterialCommunityIcons',
    icon: 'account-outline',
    component: Account,
  },
  {
    route: 'favorites',
    label: 'Αγαπημένα',
    type: 'MaterialCommunityIcons',
    icon: 'heart-outline',
    component: Favorites,
  }
];
const TabArrLoggedOut = [
  {
    route: 'stores',
    label: 'Καταστήματα',
    type: 'MaterialCommunityIcons',
    icon: 'store-outline',
    component: Stores,
  },
  {
    route: 'search',
    label: 'Αναζήτηση',
    type: 'MaterialCommunityIcons',
    icon: 'magnify',
    component: SearchStores,
  },
  {
    route: 'account',
    label: 'Λογαριασμός',
    type: 'MaterialCommunityIcons',
    icon: 'account-outline',
    component: Account,
  }
];
const Tab = createBottomTabNavigator();

const animate1 = {
  0: {scale: 0.5, translateY: 7},
  0.92: {translateY: -34},
  1: {scale: 1.2, translateY: -24},
};
const animate2 = {
  0: {scale: 1.2, translateY: -24},
  1: {scale: 1, translateY: 7},
};

const circle1 = {
  0: {scale: 0},
  0.3: {scale: 0.9},
  0.5: {scale: 0.2},
  0.8: {scale: 0.7},
  1: {scale: 1},
};
const circle2 = {0: {scale: 1}, 1: {scale: 0}};

const TabButton = props => {
  const {item, onPress, accessibilityState} = props;
  const focused = accessibilityState.selected;
  const viewRef = useRef(null);
  const circleRef = useRef(null);
  const textRef = useRef(null);

  useEffect(() => {
    if (focused) {
      viewRef.current.animate(animate1);
      circleRef.current.animate(circle1);
      textRef.current.transitionTo({scale: 1});
    } else {
      viewRef.current.animate(animate2);
      circleRef.current.animate(circle2);
      textRef.current.transitionTo({scale: 0});
    }
  }, [focused]);

  return (
    <TouchableOpacity
      onPress={onPress}
      activeOpacity={1}
      style={styles.container}>
      <Animatable.View ref={viewRef} duration={1000} style={styles.container}>
        <View style={styles.btn}>
          <Animatable.View ref={circleRef} style={styles.circle} />
          <Icon
            name={item.icon}
            color={focused ? appColors.black : appColors.mainGray}
            size={26}
            direction={'ltr'}
          />
        </View>
        <Animatable.Text ref={textRef} style={styles.text}>
          {item.label}
        </Animatable.Text>
      </Animatable.View>
    </TouchableOpacity>
  );
};

export default function AnimTab1() {
  const [routeArray, setRouteArray] = useState(TabArrLoggedOut);
  const isLoggedIn = useSelector(state => state.actions.isLoggedIn);

  useEffect(() => {
    setRouteArray(isLoggedIn ? TabArr : TabArrLoggedOut);
  }, [isLoggedIn]);

  return (
        <Tab.Navigator
          screenOptions={{
            headerShown: false,
            tabBarStyle: [
              styles.tabBar,
              {display:'flex'},
            ],
          }}>
          {routeArray.map((item, index) => {
            return (
              <Tab.Screen
                key={index}
                name={item.route}
                component={item.component}
                options={{
                  tabBarShowLabel: false,
                  tabBarButton: props => <TabButton {...props} item={item} />,
                }}
              />
            );
          })}
        </Tab.Navigator>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  tabBar: {
    height: 70,
    position: 'absolute',
    paddingHorizontal: 10,
    bottom: 10,
    right: 10,
    left: 10,
    backgroundColor: appColors.mainWhite,
    opacity: 0.9,
    borderRadius: 15,
  },
  btn: {
    width: 50,
    height: 50,
    borderRadius: 25,
    borderWidth: 0,
    borderColor: appColors.blue,
    backgroundColor: appColors.transparent,
    justifyContent: 'center',
    alignItems: 'center',
  },
  circle: {
    ...StyleSheet.absoluteFillObject,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: appColors.mainWhite,
    borderRadius: 25,
  },
  text: {
    fontSize: 10,
    textAlign: 'center',
    fontFamily: fonts.light,
    color: appColors.black,
  },
});
