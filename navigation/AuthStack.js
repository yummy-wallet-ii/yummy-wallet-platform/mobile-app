import React from 'react';
import {
  CardStyleInterpolators,
  createStackNavigator,
} from '@react-navigation/stack';
import {SplashScreen} from '../appScreens/authScreens/SplashScreen';
import {AppBoard} from '../appScreens/authScreens/AppBoard';
import {Help} from '../appScreens/authScreens/Help';
import Dashboard from '../appScreens/mainScreens/Dashboard';
import StoreDetails from '../appScreens/mainScreens/StoreDetails';
import Register from '../appScreens/authScreens/Register';
import QRCode from '../appScreens/mainScreens/QRCode';
import Transactions from '../appScreens/mainScreens/Transactions';
import Card from '../appScreens/mainScreens/Card';
import Products from '../appScreens/mainScreens/Products';
import ShoppingCart from '../appScreens/mainScreens/ShoppingCart';
import Orders from '../appScreens/mainScreens/Orders';
import StoresMap from '../appScreens/mainScreens/StoresMap';
import ExternalLogin from '../appScreens/authScreens/ExternalLogin';
import ExternalLogout from '../appScreens/authScreens/ExternalLogout';
import RedeemRequests from "../appScreens/mainScreens/RedeemRequests";

export const AuthStack = () => {
  const Stack = createStackNavigator();
  return (
    <Stack.Navigator screenOptions={{headerShown: false}}>
      <Stack.Screen
        options={{
          gestureDirection: 'horizontal',
          cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
        }}
        name="splash"
        component={SplashScreen}
      />

      <Stack.Screen
          options={{
              gestureDirection: 'horizontal',
              cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
          }}
          name="appBoard"
          component={AppBoard}
      />
      <Stack.Screen
        options={{
          gestureDirection: 'horizontal',
          cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
        }}
        name="dashboard"
        component={Dashboard}
      />

      <Stack.Screen
        options={{
          gestureDirection: 'horizontal',
          cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
        }}
        name="help"
        component={Help}
      />
      <Stack.Screen
        options={{
          headerShown: false,
          headerTransparent: true,
          gestureDirection: 'vertical',
          cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
        }}
        name="details"
        component={StoreDetails}
      />

      <Stack.Screen
        options={{
          headerShown: false,
          headerTransparent: true,
          gestureDirection: 'vertical',
          cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
        }}
        name="products"
        component={Products}
      />

      <Stack.Screen
        options={{
          headerShown: false,
          headerTransparent: true,
          gestureDirection: 'vertical',
          cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
        }}
        name="cart"
        component={ShoppingCart}
      />
      <Stack.Screen
        options={{
          headerShown: false,
          headerTransparent: true,
          gestureDirection: 'vertical',
          cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
        }}
        name="orders"
        component={Orders}
      />
        <Stack.Screen
            options={{
                headerShown: false,
                headerTransparent: true,
                gestureDirection: 'vertical',
                cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
            }}
            name="redeem-requests"
            component={RedeemRequests}
        />
      <Stack.Screen
        options={{
          headerShown: false,
          headerTransparent: true,
          gestureDirection: 'vertical',
          cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
        }}
        name="register"
        component={Register}
      />
      <Stack.Screen
        options={{
          headerShown: false,
          headerTransparent: true,
          gestureDirection: 'vertical',
          cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
        }}
        name="external-login"
        component={ExternalLogin}
      />
      <Stack.Screen
        options={{
          headerShown: false,
          headerTransparent: true,
          gestureDirection: 'vertical',
          cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
        }}
        name="external-logout"
        component={ExternalLogout}
      />
      <Stack.Screen
        options={{
          headerShown: false,
          headerTransparent: true,
          gestureDirection: 'vertical',
          cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
        }}
        name="qrCode"
        component={QRCode}
      />
      <Stack.Screen
        options={{
          headerShown: false,
          headerTransparent: true,
          gestureDirection: 'vertical',
          cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
        }}
        name="transactions"
        component={Transactions}
      />

      <Stack.Screen
        options={{
          headerShown: false,
          headerTransparent: true,
          gestureDirection: 'vertical',
          cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
        }}
        name="maps"
        component={StoresMap}
      />

      <Stack.Screen
        options={{
          headerShown: false,
          headerTransparent: true,
          gestureDirection: 'horizontal',
          cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
        }}
        name="card"
        component={Card}
      />
    </Stack.Navigator>
  );
};
