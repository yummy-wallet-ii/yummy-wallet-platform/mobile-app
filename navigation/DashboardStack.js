import React from 'react';
import {
  CardStyleInterpolators,
  createStackNavigator,
} from '@react-navigation/stack';
import SearchStores from '../appScreens/mainScreens/SearchStores';
import Favorites from '../appScreens/mainScreens/Favorites';
import Stores from '../appScreens/mainScreens/Stores';
import Account from '../appScreens/authScreens/Account';
import ScanPage from '../appScreens/mainScreens/ScanPage';

const Stack = createStackNavigator();

export const DashboardStack = () => {
  return (
    <Stack.Navigator screenOptions={{gestureEnabled: false}}>
      <Stack.Screen
        name="stores"
        component={Stores}
        options={{
          headerShown: false,
          headerTransparent: true,
          cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
        }}
      />
      <Stack.Screen
        options={{
          headerShown: false,
          headerTransparent: true,
          gestureDirection: 'horizontal',
          cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
        }}
        name="account"
        component={Account}
      />

      <Stack.Screen
        options={{
          headerShown: false,
          headerTransparent: true,
          gestureDirection: 'horizontal',
          cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
        }}
        name="search"
        component={SearchStores}
      />

      <Stack.Screen
        options={{
          headerShown: false,
          headerTransparent: true,
          gestureDirection: 'horizontal',
          cardStyleInterpolator: CardStyleInterpolators.forVerticalIOS,
        }}
        name="scan"
        component={ScanPage}
      />

      <Stack.Screen
        options={{
          headerShown: false,
          headerTransparent: true,
          gestureDirection: 'vertical',
          cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
        }}
        name="favorites"
        component={Favorites}
      />
    </Stack.Navigator>
  );
};
